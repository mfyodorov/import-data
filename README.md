# Preparing data and import it to Bitrix or send to webservice

This tool helps to extract data from file (or create own data), processing it and import data into different Bitrix entities (iblock elements, users, highload blocks etc) or send to webservice under full control.

## Features:

First step - reading from file or creation data and analyzing it, preparation for import.

* reading local or remote file to a json/create data by input fields.
* GUI for analyzing and processing every value.
* you can handle values using some PHP functions or custom methods (optionally).
* you can assign every value width some identifier for Bitrix entities or own keys.

Second step - configuration and doing import to Bitrix/sending data to webservice

* different types and settings for sending data: SOAP, HTTP GET or POST.
* two action types of importing process to Bitrix: adding new and update existing
* works with iblock elements, table of users, highload-block entities
* authomatic step-by-step import: large data file is not a problem.
* you can compare importing data and existing without update (pre-analyze).
* import configuration.
* full result statistics.
