<?
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Diag\Debug,
	Bitrix\Main\Application,
	Bitrix\Main\Type;

$request = Application::getInstance()->getContext()->getRequest();
	
/** Assets, navigation */
require_once('inc/header.php');

/** Ajax requests */
BitrixData\Prepare::ajax();

/**
 * Starting process
 */	
if($request['run'] == 'Y'):

	$time['start'] = new Type\DateTime();	
	$result = BitrixData\Prepare::createData($params, false);	
	$time['end'] = new Type\DateTime();
?>
	<p>Процесс завершен: <?=$time['start']->format('H:i:s')?> &mdash; <?=$time['end']->format('H:i:s')?></p>
		
	<?if($result['SUCCESS']):
		$res = BitrixData\Main::setData($result['DATA']);
	?>
		<?if($res !== false):?>
			<p>Массив данных обработан и сохранен. Обработано строк: <?=count($result['DATA'])?></p>
			<p>Пример:</p>
			<pre><?print_r(current($result['DATA']));?></pre>
		<?else:?>
			<p>Ошибка записи</p>
		<?endif;?>
		
	<?else:?>
		<p>Не удалось прочитать файл</p>
	<?endif;?>
	
	<p>-----------------------------------------</p>
	
<?endif;?>

<form action="" id="createForm" class="main-form" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="run" value="Y" />
	<div class="row tabs" show-step="1">
		<label for="">Источник</label>
		<input type="radio" name="source" id="radio-file" value="file" <?if($params['source'] == 'file') echo 'checked';?> /><label for="radio-file">Чтение из файла</label>
		<input type="radio" name="source" id="radio-bitrix" value="bitrix" <?if($params['source'] == 'bitrix') echo 'checked';?> /><label for="radio-bitrix">Получить из Битрикса</label>
		<input type="radio" name="source" id="radio-new" value="new" <?if($params['source'] == 'new') echo 'checked';?> /><label for="radio-new">Создать данные</label>
	</div>
	<div class="block aclear" show-step="1">
		<fieldset show-source="file" class="show-depend">
			<div class="row">
				<input name="file" id="file" size="20" type="file" />
			</div>
			<div class="row">
				<input type="text" name="file_url" class="full" value="<?=$params['file_url']?>" placeholder="Или укажите ссылку на файл" />
			</div>
			<div class="row">
				<label for="">Тип файла</label>
				<select name="file_type" class="medium">
					<?foreach(BitrixData\Prepare::getFileTypes() as $name => $text):?>
						<option value="<?=$name?>" <?if($params['file_type'] == $name) echo 'selected';?>><?=$text?></option>
					<?endforeach;?>
				</select>
			</div>
		</fieldset>
		<fieldset show-source="bitrix" class="show-depend">
			<div class="row">
				<label for="">Объект</label>
				<select name="bitrix_entity" class="medium" data-bind-target="object">
				<?foreach(BitrixData\Main::getObjects() as $name => $text):?>
					<option value="<?=$name?>" <?if($params['bitrix_entity'] == $name) echo 'selected';?>><?=$text?></option>
				<?endforeach;?>
				</select>
			</div>
			<div class="row show-depend" show-bitrix_entity="ELEMENT,HL">
				<label for="">ID блока</label>
				<input type="text" name="bitrix_block_id" class="medium" value="<?=$params['bitrix_block_id']?>" />
			</div>
			<div class="row show-depend">
				<label for="">Поля фильтра</label>
				<div class="repeat-group">
					<?foreach($params['bitrix_filter'] as $id => $val):?>
						<div class="item" data-id="<?=$id?>">
							<input type="text" class="medium" name="bitrix_filter[<?=$id?>][name]" value="<?=$val['name']?>" placeholder="ключ" />
							<input type="text" class="medium" name="bitrix_filter[<?=$id?>][value]" value="<?=$val['value']?>" placeholder="значение" />
							<select name="bitrix_filter[<?=$id?>][type]" class="medium">
								<option value="string"<?if($val['type'] == 'string'):?> selected<?endif;?>>строка</option>
								<option value="int"<?if($val['type'] == 'int'):?> selected<?endif;?>>число</option>
								<option value="array"<?if($val['type'] == 'array'):?> selected<?endif;?>>массив</option>
							</select>
						</div>
					<?endforeach;?>
					<span class="add add-field" title="Добавить">+</span>
				</div>
			</div>
			<div class="row show-depend">
				<label for="">Поля выборки</label>
				<div class="repeat-group">
					<?foreach($params['bitrix_select'] as $id => $val):?>
						<div class="item" data-id="<?=$id?>">
							<input type="text" class="medium" name="bitrix_select[<?=$id?>][name]" value="<?=$val['name']?>" placeholder="поле или свойство" />
							<input type="text" class="medium" name="bitrix_select[<?=$id?>][alias]" value="<?=$val['alias']?>" placeholder="алиас" />
							<select show-bitrix_entity="ELEMENT" name="bitrix_select[<?=$id?>][return]" class="medium show-depend">
								<?foreach(BitrixData\Prepare::getFieldReturnKeys('ELEMENT') as $name => $text):?>
									<option value="<?=$name?>" <?if($val['return'] == $name) echo 'selected';?>><?=$text?></option>
								<?endforeach;?>
							</select>
							<select show-bitrix_entity="HL,USER,TICKET" name="bitrix_select[<?=$id?>][return]" class="medium show-depend" disabled>
								<?foreach(BitrixData\Prepare::getFieldReturnKeys('HL') as $name => $text):?>
									<option value="<?=$name?>" <?if($val['return'] == $name) echo 'selected';?>><?=$text?></option>
								<?endforeach;?>
							</select>
						</div>
					<?endforeach;?>
					<span class="add add-field" title="Добавить">+</span>
				</div>
			</div>
			<div class="row show-depend">
				<label for="">Лимит выборки</label>
				<input type="text" name="bitrix_limit" class="medium" value="<?=$params['bitrix_limit']?>" />
			</div>
		</fieldset>
	</div>
	<fieldset show-step="1">
		<div class="title">Обработка полей</div>
		<div class="row">
			<label for="">Объект</label>
			<select name="object" class="medium">
				<option value="">-- не выбран --</option>
				<?foreach(BitrixData\Main::getObjects() as $name => $text):?>
					<option value="<?=$name?>" <?if($params['object'] == $name) echo 'selected';?>><?=$text?></option>
				<?endforeach;?>
			</select>
		</div>
	</fieldset>
	<div id="preview"></div>
	<fieldset show-step="2">
		<div class="row">
			<label for="">Лимит строк</label>
			<input type="text" name="limit_items" class="medium" value="<?=$params['limit_items']?>" />
		</div>
		<div class="row">
			<label for="">Исключить строки</label>
			<input type="text" name="exclude_rows" class="medium" value="<?=$params['exclude_rows']?>" />
		</div>
		<div class="row">
			<label for="">Исключения</label>
			<div class="repeat-group">
				<?foreach($params['exclude'] as $id => $arr):?>
					<div class="item" data-id="<?=$id?>">
						<input type="text" class="medium exclude-src" value="<?=(strlen($arr['value']) > 0 ? (int)$arr['value']+1 : $arr['value'])?>" placeholder="номер поля" />
						<input type="hidden" name="exclude[<?=$id?>][name]" class="exclude-dest" value="<?=$arr['value']?>" />
						<select name="exclude[<?=$id?>][type]" class="medium">
							<?foreach(BitrixData\Main::getComparingTypes() as $key => $value):?>
								<option value="<?=$key?>" <?if($arr['type'] == $key) echo 'selected';?>><?=$value?></option>
							<?endforeach;?>
						</select>
						<input type="text" name="exclude[<?=$id?>][value]" class="medium" value="<?=$arr['value']?>" placeholder="значение" />
					</div>
				<?endforeach;?>
				<span class="add add-field" title="Добавить">+</span>
			</div>
		</div>
	</fieldset>
	<input type="submit" value="Продолжить" />
</form>

<script>
$(function() {	
	prepareData.initCreate();	
});
</script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>