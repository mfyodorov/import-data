<?
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Diag\Debug,
	Bitrix\Main\Application;
	
$request = Application::getInstance()->getContext()->getRequest();

$to = (in_array($request['to'], array('bitrix','webservice')) ? $request['to'] : 'bitrix');

/** Assets, navigation */
require_once('inc/header.php');

$convertCharset = false;

if ($request['unset'] == 'Y') 
{	
	if (isset($_SESSION['MISHA']))
	{
		unset($_SESSION['MISHA']);
		echo '<p>Сессия сброшена</p>';
	} 
	else echo '<p>Сессия не инициализирована</p>';
}

if ($request['run'] == 'Y')
{	
	/** Init session */
	if (!isset($_SESSION['MISHA'])) 
	{
		$STEP = 0; $IN = 0; $OUT = 0;
		$_SESSION['MISHA']['RESULT'] = false;
		$_SESSION['MISHA']['STEP'] = null;
		$_SESSION['MISHA']['ACTION'] = null;
		$_SESSION['MISHA']['STAT'] = ['IN' => null, 'OUT' => null, 'LAST' => null, 'ALL' => null];
	}
	else 
	{
		$STEP = $_SESSION['MISHA']['STEP'];
		$IN = $_SESSION['MISHA']['STAT']['IN'];
		$OUT = $_SESSION['MISHA']['STAT']['OUT'];
		$ACTION = $_SESSION['MISHA']['ACTION'];
	}

	/** If process is finished */
	if ($_SESSION['MISHA']['RESULT'] === true) 
	{
		BitrixData\Main::setData($_SESSION['MISHA'], BD_RESULT_FILE);
	} 
	else
	{
		/** Get data from file */
		$arJson = (
			$STEP == 0
			? BitrixData\Main::getData()
			: BitrixData\Main::getData(BD_TEMP_FILE)
		);
		
		/** If limit of items is set */
		if ($STEP == 0 && intval($params['limit_items']) > 0 && count($arJson) > intval($params['limit_items'])) 
			$arJson = array_slice($arJson, 0, $params['limit_items']);
		
		/** Process */
		if (is_array($arJson))
		{
			
			if (count($arJson) > $params['astep'])
			{
				$curArJson = array_slice($arJson, 0, $params['astep']); /** Elements for current step */
				$newArJson = array_slice($arJson, $params['astep']); /** Left elements */
				
				$writting = BitrixData\Main::setData($newArJson, BD_TEMP_FILE);
				
				if ($writting === false)
				{
					$_SESSION['MISHA']['STOP'] = true;
					$_SESSION['MISHA']['ERROR'] = 'Cannot write to temporary file';
				}
			}
			else 
			{
				$curArJson = $arJson;
				$_SESSION['MISHA']['RESULT'] = true;
			}
			
			if (!$_SESSION['MISHA']['STOP'])
			{
				++$STEP;
				
				$stepIn = count($curArJson);
				
				/** Action */
				$count = 0;
				$actionResult = true;
				foreach ($curArJson as $k => $arItem)
				{
					/** Проверяем условия на исключение из процесса */
					$exclude = BitrixData\Main::checkExclude($arItem, $params['exclude']);
					if ($exclude['SUCCESS'])
					{
						$id = ($params['filter_by'] ? $arItem[$params['filter_by']] : $arItem['ID']);
						$_SESSION['MISHA']['STAT']['EXCLUDED'][] = sprintf('#%s: "%s" %s "%s"',
							($id ? $id : $IN+$k),
							$exclude['VALUE'],
							$exclude['EXCLUDE']['type'],
							$exclude['EXCLUDE']['value']
						);
						continue;
					}
					
					if ($to == 'bitrix')
						$actionResult = BitrixData\Import::toBitrix($arItem, $params, $ACTION, $count, $IN);
					elseif ($to == 'webservice')
						$actionResult = BitrixData\Import::toWebservice($arItem, $params, $ACTION, $count, $IN);
					
					if (!$actionResult && $params['error_stop'] == 'Y')
					{
						$_SESSION['MISHA']['STOP'] = true;
						$_SESSION['MISHA']['ERROR'] = 'Error on step #'.$STEP.', process was stopped by user';
						break;
					}
				}
				
				$IN = $IN + $stepIn;
				$OUT = $OUT + $count;
			}			
		}
		else
		{
			$_SESSION['MISHA']['STOP'] = true;
			
			if (json_last_error())
				$_SESSION['MISHA']['ERROR'] = 'Error reading json (#'.json_last_error().')';
			else
				$_SESSION['MISHA']['ERROR'] = 'Error reading file (file is empty or doesn\'t exist)';
		}

		$_SESSION['MISHA']['STEP'] = $STEP;
		$_SESSION['MISHA']['STAT']['IN'] = $IN;
		$_SESSION['MISHA']['STAT']['OUT'] = $OUT;
		$_SESSION['MISHA']['STAT']['LAST'] = count($newArJson);
		if (!isset($_SESSION['MISHA']['STAT']['ALL']))
			$_SESSION['MISHA']['STAT']['ALL'] = count($arJson);		
		if (!empty($ACTION))
			$_SESSION['MISHA']['ACTION'] = $ACTION;
	}
}

/** Print response */
if ($request['isAjax'] == true)
{
	global $APPLICATION, $convertCharset;
	$APPLICATION->RestartBuffer();
	if ($convertCharset)
	{
		$arSessionResult = $APPLICATION->ConvertCharsetArray($_SESSION['MISHA'], "windows-1251", "utf-8");
	}
	else $arSessionResult = $_SESSION['MISHA'];
	echo json_encode($arSessionResult);
	die();
}
else 
{
	if (isset($_SESSION['MISHA']))
	{
		echo '<div id="result">';
		if ($convertCharset)
		{
			global $APPLICATION;
			$arSessionResult = $APPLICATION->ConvertCharsetArray($_SESSION['MISHA'], "windows-1251", "utf-8");
		}
		else $arSessionResult = $_SESSION['MISHA'];
		echo strip_tags(json_encode($arSessionResult)); 
		echo '</div>';		
	}
	
	if ($request['run'] == 'Y')
	{
	?>
		<script>
			$(document).ready(function()
			{
				<?if (!$_SESSION['MISHA']['STOP']):?>
					ajaxRequest();
				<?else:?>
					alert('<?=$_SESSION['MISHA']['ERROR']?>');
				<?endif;?>
				function ajaxRequest()
				{
					/* 
					Просто перезагружать страницу
					setTimeout(function(){
						window.location = '<?=$request->getRequestUri()?>';
					},1000);
					*/
					var data = <?=CUtil::PhpToJSObject($params)?>;
					data['isAjax'] = true;
					data['run'] = 'Y';
					setTimeout(function()
					{
						var jqxhr = $.post('<?=$request->getRequestUri()?>', data);
						jqxhr.done(function(data)
						{
							$obj = JSON.parse(data);
							console.log($obj);
							$('#result').html(jsonPrint($obj));
							if ($obj.RESULT)
							{
								alert('Выполнено');
							}
							else if ($obj.STOP)
							{
								alert($obj.ERROR);
							}
							else 
							{
								ajaxRequest();
								//alert('Результат: '+$obj.RESULT+' Step: '+$obj.STEP);
							}
						});
					},1000);
				}
			});
		</script>
	<?
	}
}

?>
<form action="" id="database-form" class="main-form" method="POST">
	<input type="hidden" name="run" value="Y" />
	<fieldset>
		<div class="title">Разбор данных</div>
		<div class="row">
			<label for="">Шаг разбора</label>
			<input type="text" name="astep" class="small" value="<?=$params['astep']?>" /><label for="">строк</label>
		</div>
		<div class="row">
			<label for="">Лимит строк</label>
			<input type="text" name="limit_items" class="small" value="<?=$params['limit_items']?>" />
		</div>
		<div class="row">
			<label for="">Исключения</label>
			<div class="repeat-group">
				<?foreach ($params['exclude'] as $id => $arr):?>
					<div class="item" data-id="<?=$id?>">
						<select name="exclude[<?=$id?>][name]" class="medium">
							<option value="">-- параметр --</option>
							<?foreach (BitrixData\Main::getDataKeys(BD_DATA_FILE) as $key):?>
								<option value="<?=$key?>" <?if ($arr['name'] == $key) echo 'selected';?>><?=$key?></option>
							<?endforeach;?>
						</select>
						<select name="exclude[<?=$id?>][type]" class="medium">
							<?
							foreach (BitrixData\Main::getComparingTypes() as $key => $value):
							?>
								<option value="<?=$key?>" <?if ($arr['type'] == $key) echo 'selected';?>><?=$value?></option>
							<?
							endforeach;
							?>
						</select>
						<input type="text" name="exclude[<?=$id?>][value]" class="medium" value="<?=$arr['value']?>" placeholder="значение" />
					</div>
				<?endforeach;?>
				<span class="add add-field" title="Добавить">+</span>
			</div>
		</div>
	</fieldset>
	<?require_once('inc/form-'.$to.'.php');?>
	<input type="submit" value="Старт" />
</form>
<form action="" method="POST">
	<input type="hidden" name="unset" value="Y" />
	<input type="submit" value="Сбросить сессию" />
</form>

<script>
var str = $("#result").text();
if (str)
{
	var json = JSON.parse(str);
	$("#result").html(jsonPrint(json));
}
</script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>