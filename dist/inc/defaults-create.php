<?
$defaults['source'] = 'file';
$defaults['file_url'] = '';
$defaults['limit_items'] = '';
$defaults['exclude_rows'] = '';
$defaults['exclude'] = array(array('name' => '', 'type' => '', 'value' => ''));
$defaults['object'] = '';
$defaults['file_type'] = 'csv_comma';

$defaults['bitrix_entity'] = 'ELEMENT';
$defaults['bitrix_block_id'] = '';
$defaults['bitrix_select'] = array(
	array('name' => '', 'return' => 'VALUE'),
);
$defaults['bitrix_filter'] = array(
	array('name' => '', 'value' => '', 'type' => 'string'),
);
$defaults['bitrix_limit'] = '';