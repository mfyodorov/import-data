<?
$defaults['astep'] = 20;
$defaults['limit_items'] = '';
$defaults['exclude'] = array(array('name' => '', 'type' => '', 'value' => ''));

$defaults['object'] = 'USER';
$defaults['action'] = 'UPD';
$defaults['entity_id'] = '';
$defaults['iblock_id'] = 1;
$defaults['filter_by'] = 'ID';
$defaults['update'] = '';
$defaults['empty'] = '';
$defaults['login'] = '';
$defaults['generate_pass'] = '';
$defaults['php_code_order'] = '';
$defaults['php_code_before'] = '';
$defaults['php_code_after'] = '';