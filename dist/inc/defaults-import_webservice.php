<?
$defaults['astep'] = 1;
$defaults['limit_items'] = 3;
$defaults['exclude'] = array(array('name' => '', 'type' => '', 'value' => ''));

$defaults['protocol'] = 'POST';
$defaults['url'] = '';
$defaults['method'] = '';
$defaults['auth'] = '';
$defaults['login'] = '';
$defaults['password'] = '';
$defaults['headers'] = array(array('presetId' => '', 'name' => '', 'value' => ''));
$defaults['error_stop'] = '';
$defaults['decode_json'] = '';
$defaults['php_code_order'] = '';
$defaults['php_code_before'] = '';
$defaults['php_code_after'] = '';