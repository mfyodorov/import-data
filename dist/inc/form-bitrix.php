<fieldset>
	<div class="title">Обработка данных</div>
	<div class="row">
		<label for="">Объект</label>
		<select name="object" class="medium">
			<?foreach(BitrixData\Main::getObjects() as $name => $text):?>
				<option value="<?=$name?>" <?if($params['object'] == $name) echo 'selected';?>><?=$text?></option>
			<?endforeach;?>
		</select>
	</div>
	<div class="row tabs">
		<label for="">Действие</label>
		<input type="radio" name="action" id="radio-upd" value="UPD" <?if($params['action'] == 'UPD') echo 'checked';?> /><label for="radio-upd">Поиск и обновление</label>
		<input type="radio" name="action" id="radio-add" value="ADD" <?if($params['action'] == 'ADD') echo 'checked';?> /><label show-object="USER,ELEMENT,HL" for="radio-add" class="show-depend">Добавление новых</label>
	</div>
	<div class="block aclear">
		<fieldset>
			<div class="row show-depend" show-object="ELEMENT,HL">
				<label for="">ID блока</label><input type="text" name="iblock_id" class="medium" value="<?=$params['iblock_id']?>" />
			</div>
			<div class="row show-depend" show-object="UF">
				<label for="">ID сущности</label><input type="text" name="entity_id" class="medium" value="<?=$params['entity_id']?>" />
			</div>
			<div class="row show-depend" show-action="UPD">
				<label for="">Параметр фильтрации</label>
				<select name="filter_by" class="medium">
					<?foreach(BitrixData\Main::getDataKeys(BD_DATA_FILE) as $key):?>
						<option value="<?=$key?>" <?if($params['filter_by'] == $key) echo 'selected';?>><?=$key?></option>
					<?endforeach;?>
				</select>
			</div>
			<div class="row row-long show-depend" show-action="UPD">
				<input type="checkbox" id="cbox-update" name="update" value="Y" <? if($params['update'] == "Y") echo "checked"; ?> />
				<label for="cbox-update">Обновить поля </label>
			</div>
			<div class="row row-long show-depend" show-action="UPD">
				<input type="checkbox" id="cbox-empty" name="empty" value="Y" <? if($params['empty'] == "Y") echo "checked"; ?> />
				<label for="cbox-empty">Записывать пустое значение </label>
			</div>
			<div class="row row-long show-depend" show-action="ADD" show-object="USER">
				<input type="checkbox" id="cbox-login" name="login" value="Y" <? if($params['login'] == "Y") echo "checked"; ?> />
				<label for="cbox-login">Логин как email </label>
			</div>
			<div class="row row-long show-depend" show-action="ADD" show-object="USER">
				<input type="checkbox" id="cbox-generate_pass" name="generate_pass" value="Y" <? if($params['generate_pass'] == "Y") echo "checked"; ?> />
				<label for="cbox-generate_pass">Генерировать пароль </label>
			</div>
		</fieldset>
	</div>
</fieldset>
<?require('form-php-code.php')?>