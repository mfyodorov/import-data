<fieldset>
	<div class="title">Произвольный PHP код</div>
	<div class="row tabs">
		<label for="">Выполнить</label>
		<input 
			type="radio" 
			name="php_code_order" 
			id="php_code_order--before" 
			value="before" 
			<?if($params['php_code_order'] == 'before') echo 'checked';?> 
		/>
		<label for="php_code_order--before">
			До 
			<?if (BD_CUR_PAGE == 'import_bitrix'):?><span show-action="UPD">обновления</span><span show-action="ADD">добавления</span><?endif;?>
			<?if (BD_CUR_PAGE == 'import_webservice'):?>отправки<?endif;?>
		</label>
		<input 
			type="radio" 
			name="php_code_order" 
			id="php_code_order--after" 
			value="after" 
			<?if($params['php_code_order'] == 'after') echo 'checked';?> 
		/>
		<label for="php_code_order--after">
			После 
			<?if (BD_CUR_PAGE == 'import_bitrix'):?><span show-action="UPD">обновления</span><span show-action="ADD">добавления</span><?endif;?>
			<?if (BD_CUR_PAGE == 'import_webservice'):?>отправки<?endif;?>
		</label>
	</div>
	<div class="block aclear">
		<fieldset class="show-depend" show-php_code_order="before">
			<div class="row row-long">
				<textarea name="php_code_before" id="php_code_before" placeholder="Доступна переменная $item"><?=$params['php_code_before']?></textarea>
			</div>
		</fieldset>
		<fieldset class="show-depend" show-php_code_order="after">
			<div class="row row-long">
				<textarea name="php_code_after" id="php_code_after" placeholder="Доступна переменная $item"><?=$params['php_code_after']?></textarea>
			</div>
		</fieldset>
	</div>
</fieldset>