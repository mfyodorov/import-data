<fieldset>
	<div class="title">Параметры запроса</div>
	<div class="row">
		<label for="">Протокол</label>
		<select name="protocol" class="medium">
			<?foreach(BitrixData\Main::getRequestTypes() as $name => $text):?>
				<option value="<?=$name?>" <?if($params['protocol'] == $name) echo 'selected';?>><?=$text?></option>
			<?endforeach;?>
		</select>
	</div>
	<div class="row">
		<label for="">Url</label>
		<input type="text" name="url" value="<?=$params['url']?>" />
	</div>
	<div class="row show-depend" show-protocol="SOAP">
		<label for="">Метод</label>
		<input type="text" name="method" value="<?=$params['method']?>" />
	</div>
	<div class="row show-depend">
		<label for="">Авторизация</label>
		<select name="auth" class="medium">
			<?foreach(BitrixData\Main::getAuthTypes() as $name => $text):?>
				<option value="<?=$name?>" <?if($params['auth'] == $name) echo 'selected';?>><?=$text?></option>
			<?endforeach;?>
		</select>
		<div class="row show-depend" show-auth="basic">
			<input type="text" name="login" class="medium" value="<?=$params['login']?>" placeholder="Пользователь" />
			<input type="text" name="password" class="medium" value="<?=$params['password']?>" placeholder="Пароль" />
		</div>
	</div>
	<div class="row show-depend" show-protocol="GET,POST">
		<label for="">Заголовки</label>
		<div class="repeat-group">
			<?foreach ($params['headers'] as $id => $arr):?>
				<div class="item" data-id="<?=$id?>">
					<select name="headers[<?=$id?>][presetId]" class="medium preset-field">
						<?foreach(BitrixData\Main::getHttpHeaders() as $presetId => $item):?>
							<option 
								value="<?=$presetId?>" 
								<?if($item['name']):?>
									data-name="<?=$item['name']?>"
									data-value="<?=$item['value']?>"
								<?endif;?>
								<?if($arr['presetId'] == $presetId) echo 'selected';?>
							>
								<?=($item['label'] ?: $presetId)?>
							</option>
						<?endforeach;?>
					</select>
					<input type="text" name="headers[<?=$id?>][name]" class="medium" value="<?=$arr['name']?>" placeholder="имя" />
					<input type="text" name="headers[<?=$id?>][value]" class="medium" value="<?=$arr['value']?>" placeholder="значение" />
				</div>
			<?endforeach;?>
			<span class="add add-field" title="Добавить">+</span>
		</div>
	</div>
</fieldset>
<fieldset>
	<div class="title">Обработка ответа</div>
	<div class="row show-depend">
		<input type="checkbox" id="cbox-decode_json" name="decode_json" value="Y" <? if($params['decode_json'] == "Y") echo "checked"; ?> />
		<label for="cbox-decode_json">JSON </label>
	</div>
</fieldset>
<fieldset>
	<div class="row row-long">	
		<input type="checkbox" name="error_stop" id="cbox_error_stop" value="Y" <? if($params['error_stop'] == "Y") echo "checked"; ?> />
		<label for="cbox_error_stop">Прервать процесс при возникновении ошибки</label>
	</div>
</fieldset>
<?require('form-php-code.php')?>