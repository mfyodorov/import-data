<?
use Bitrix\Main\Application,
	Bitrix\Main\Diag\Debug;

set_time_limit(0);
	
$request = Application::getInstance()->getContext()->getRequest();

/** For Windows OS */
$dir = str_replace(DIRECTORY_SEPARATOR, '/', __DIR__);

define(BD_DIR_ROOT, preg_replace('#/inc$#', '', $dir));
define(BD_DIR, str_replace(Application::getDocumentRoot(), '', BD_DIR_ROOT));
define(BD_DIR_URL, ($request->isHttps() ? 'https://' : 'http://') . $request->getHttpHost(). BD_DIR);

define(BD_DATA_FILE, BD_DIR_ROOT . '/data/data.json');
define(BD_DATA_FILE_URL, BD_DIR_URL . '/data/data.json');
define(BD_TEMP_FILE, BD_DIR_ROOT . '/data/temp.json');
define(BD_RESULT_FILE, BD_DIR_URL . '/data/result.json');

$pages = array(
	'create' => 'create.php',
	'process' => 'process.php',
	'import_bitrix' => 'import.php?to=bitrix',
	'import_webservice' => 'import.php?to=webservice',
);
foreach($pages as $name => $url)
{
	if(strpos($request->getRequestUri(), $url) !== false)
		define(BD_CUR_PAGE, $name);
}

$classes = array(
	'BitrixData\Main' => BD_DIR.'/lib/main.php',
	'BitrixData\Prepare' => BD_DIR.'/lib/prepare.php',
	'BitrixData\Import' => BD_DIR.'/lib/import.php',
	'BitrixData\HLBlocks' => BD_DIR.'/lib/hlblocks.php',
);
\CModule::AddAutoloadClasses('', $classes);

/** Page params */
require_once('defaults-'.BD_CUR_PAGE.'.php');

$savedForm = BitrixData\Main::getFormValues(BD_CUR_PAGE);

if(!empty($savedForm))
	$defaults = $savedForm;

foreach($defaults as $name => $value)
{
	$params[$name] = ($request['run'] == 'Y' ? $request[$name] : $defaults[$name]);
}

$check = BitrixData\Main::checkParams(BD_CUR_PAGE, $params);
if(!$check['SUCCESS'])
	die($check['MESSAGE']);

BitrixData\Main::saveFormValues(BD_CUR_PAGE, $params);

?>
<link rel="stylesheet" href="css/main.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="js/formState.js"></script>
<script src="js/script.js"></script>

<?if(strpos('import_', BD_CUR_PAGE) === 0):?>
	<script src="js/import-<?=$to?>.js"></script>
<?else:?>
	<script src="js/prepareData.js"></script>
<?endif;?>

<div class="header aclear">

	<ul class="menu">
		<li<?if(BD_CUR_PAGE == 'create'):?> class="current"<?endif?>><a href="create.php">Создание данных</a>
		<li>></li>
		<li<?if(BD_CUR_PAGE == 'process'):?> class="current"<?endif?>><a href="process.php">Обработка массива</a>
		<li>></li>
		<li<?if(BD_CUR_PAGE == 'import_bitrix'):?> class="current"<?endif?>><a href="import.php?to=bitrix">Импорт в Битрикс</a>
		<li>или</li>
		<li<?if(BD_CUR_PAGE == 'import_webservice'):?> class="current"<?endif?>><a href="import.php?to=webservice">Отправка в запросе</a>
	</ul>
	
	<?/*
	<div class="attrs">
		<span id="save-form" class="pseudo-link">Сохранить форму</span>
	</div>
	*/?>

</div>