<?
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;

/** For Windows OS */
$dir = str_replace(DIRECTORY_SEPARATOR, '/', __DIR__);

$curDir = str_replace(Application::getDocumentRoot(), '', $dir);

LocalRedirect($curDir.'/import.php?to=bitrix');
