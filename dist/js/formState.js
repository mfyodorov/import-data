var formState = {	
	
	current : {},
	
	init : function(object) {
		/** Get all attrs which start with 'show-' */
		var keys = this.getAttrs(object);
		
		for (var i in keys) {
			var elem = this.findElement(keys[i]);			
			if (elem) {
				/** Save current values into object property */
				this.current[keys[i]] = elem.val();

				/** Add event listeners to keys */
				elem.on('change', function(){
					formState.changeState($(this).data('show-id'));
				});	
			}		
		}		
		this.changeState(keys);		
	},
	
	changeState : function(types) {
		if (typeof types === 'string')
			types = [types];
		
		for (var i in types) {
			var elem = this.findElement(types[i]);
			
			if (elem.attr('type') == 'radio' || elem.attr('type') == 'checkbox')
				elem = elem.filter(':checked');
			this.current[types[i]] = elem.val();
			
			var deps = $('[show-'+types[i]+']');				
			this.toggleElement(deps, false);
			
			$.each(deps, $.proxy(
				function(i, dep){					
					this.toggleElement($(dep), this.checkField($(dep)));
				}, this)
			);
		}
	},
	
	toggleElement : function(elem, trigger) {
		if (trigger)
			elem.show();
		else
			elem.hide();
		
		if (elem.attr('name'))
			elem.attr('disabled', !trigger);
	},
	
	findElement : function(string) {
		var selector = {
			'name' : '[name="'+string+'"]',
			'id' : '#'+string,
		};		
		for (var attr in selector) {
			var elem = $(selector[attr]);
			if (elem.length) {
				elem.data('show-id', string);
				return elem;
			}
		}
		return false;
	},
	
	/** Compare conditions of the element (it's attributes values) with global current values */
	checkField : function(el) {
		var keys = this.getAttrs(el);
		
		for (var i in keys) {
			var value = el.attr('show-'+keys[i]),
				values = value.split(',');
			if (values.indexOf(this.current[keys[i]]) === -1)
				return false;
		}
		return true;
	},
	
	getAttrs : function(el) {
		var attrs = [];
		el.each(function(){
			$.each(this.attributes, function(){
				if (formState.checkAttr(this.name))
				{
					var key = this.name.substr(5);
					if (attrs.indexOf(key) === -1)
						attrs.push(key);
				}
			});
		});	
		return attrs;
	},
	
	checkAttr : function(attr) {
		return (attr.indexOf('show-') !== -1);
	}
	
};