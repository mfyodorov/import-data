$(function(){
	
	$('#database-form').on('submit',function()
	{
		var object = $('[name="object"]').val(),
			iblock_id = $('[name="iblock_id"]').val();
			
		if((object == 'ELEMENT' || object == 'HL') && !iblock_id.length)
		{
			alert('Не указан инфоблок');
			return false;
		}
		
		var update = $('[name="update"]',this).prop('checked');
		if(update)
		{
			var confirm = confirm('Вы уверены, что хотите обновить данные?');
			if(!confirm)
				$('[name="update"]',this).prop('checked',false);
			return confirm;
		}			
	});	
		
});