var prepareData = {
	
	curStep : 1,
	form : {},
	
	initCreate : function()
	{
		this.form = $('.main-form');
		
		this.init();
		
		this.form.on('submit',
			$.proxy(function(e){
				e.preventDefault();
				this.getImportFields(1);
			}, this)
		);
		
		this.form.on('change','.exclude-src',function(e)
		{
			var curVal = $(this).val(),
				key = parseInt($(this).val())-1,
				destInput = $(this).parent().find('.exclude-dest');
				
			if (parseInt(curVal) > 0)
				destInput.val(key);
			else {
				destInput.val('');
				$(this).val('');
			}
		});	
		
		var bindElemAttr = '[data-bind-target]';
		this.bindTargetHandler($(bindElemAttr, this.form), this.form);
		this.form.on('change', bindElemAttr, this.bindTargetHandler);		
		
	},
	
	init : function()
	{
		this.changeStep(1);		
	},
	
	bindTargetHandler : function(elem, form)
	{
		if (elem.target)
		{
			elem = $(this);
			form = this.form;
		}
		
		var curVal = elem.val(),
			targetName = elem.data('bind-target'),
			target;			
		if (targetName)
			target = $('[name="' + targetName + '"]', form);
		if (target)
			target.val(curVal);
	},

	changeStep : function(step)
	{
		this.curStep = step || this.curStep;
		$('[show-step]', this.form).hide().filter('[show-step="'+this.curStep+'"]').show();
		
		switch(step)
		{
			case 2: 
				this.buildPreviewForm();
				
				/** Назначение ключа значению поля */
				$('#fields-wrap').on('input', '.link', function(){
					prepareData.changeSetField($(this).closest('tr').attr('data-row'));
				})
				
				/** Изменение типа обработки значения */
				$('#fields-wrap').on('change','.process', function(e)
				{
					var rowNumber = $(this).closest('tr').attr('data-row'),
						procTypeId = $(this).parent().attr('data-id');
						
					if ($(this).hasClass('proc-type-list'))	
						prepareData.changeProcessingType(rowNumber, procTypeId);	
					else
						prepareData.processValue(rowNumber);
				});
				
				/**
				 * При выборе типа КЛАДРа, для которого нужно указать тип родителя, проходимся по значениям списка,
				 * чтобы оставить активными только вышестоящие типы (т.е. чтобы для нас. пункта можно было указать тип родителя регион или район,
				 * но не нас. пункт или улицу)
				 */	
				var kladrTypes = ['region', 'district', 'city', 'street'];
				
				$('#fields-wrap').on('change','select[data-name="kladr_type"]',function(){
					parentType = $(this).parent().find('select[data-name="kladr_parent_type"]');
					if (parentType.length){
						parentType.val('');
						val = $(this).val();
						disabled = false;
						for (var i in kladrTypes){ 
							if (kladrTypes[i] == val) disabled = true;					
							$('option[value="'+kladrTypes[i]+'"]', parentType).attr('disabled', disabled);
						}
					}
				});				
				
				/** Сохранение конфигурации */	
				$('#fields-wrap').on('click', '#save-config', function(e){
					prepareData.savePreviewValues();
				});
				
				/** Загрузка конфигурации */	
				$('#fields-wrap').on('click', '#load-config', function(e){
					prepareData.loadPreviewValues();
				});
				
				/** Добавление доп. полей по клику на кнопке */	
				$('#fields-wrap').on('click', '#add-row', function(e){
					e.preventDefault();
					prepareData.addField($(this).attr('data-key'));
				});
				
				/** Копирование поля */	
				$('#fields-wrap').on('click', '[data-copy]', function(e){
					prepareData.addField($('#add-row').attr('data-key'), '', $(this).attr('data-copy'));
				});
				
				/** Добавление нового поля обработки */	
				$('#fields-wrap').on('click', '.add-proc-type', function(e){
					var tr = $(this).closest('tr'),
						rowNumber = tr.attr('data-row'),
						key = parseInt(rowNumber)-1,
						lastProcType = $('.proc-field .item', tr).last(),
						newProcTypeId = parseInt(lastProcType.attr('data-id'))+1;
					
					var string = prepareData.getProcTypeTemplate(newProcTypeId, key, prepareData.previewData.procFieldsList);
					lastProcType.after(string);
				});
				
				$('#fields-wrap').on('click', '.add-proc-data-group', function(e){
					var tr = $(this).closest('tr'),
						rowNumber = tr.attr('data-row'),
						key = parseInt(rowNumber)-1,
						lastProcType = $('.proc-field .item', tr).last(),
						procTypeId = parseInt(lastProcType.attr('data-id')),
						
						group = $(this).closest('.proc-data-group'),
						row = parseInt(group.attr('data-row')) + 1;
					
					var string = prepareData.getProcDataFieldsGroup(procTypeId, key, row);
					group.after(string);
				});
				
			break;
		}
	},	

	getImportFields : function($string)
	{ 
		var data = new FormData(this.form[0]);
		data.append('isAjax', true);
		data.append('action', 'preview');
		data.append('string', $string);
		
		$.ajax({
			url: '',
			type: 'POST',
			data: data,
			contentType: false,
			processData: false,
			cache: false,
			context: this,
			success: function(data)
			{
				var result = JSON.parse(data);
				if (result && result.SUCCESS)
				{
					this.previewData = result;
					this.changeStep(2);
				} 
				else
				{
					alert(result.MESSAGE);
				}
			},			
			error: function(error){
				console.log(error);
			},
		});
		
	},
	
	/**
	 * Выводим таблицу назначения полей
	 */	
	buildPreviewForm : function()
	{
		var content = '';
		for (var key in this.previewData.importFieldsValues) 
		{
			var i = parseInt(key)+1,
				valueField = this.getValueFieldTemplate(key, this.previewData.importFieldsValues[key]),
				setFieldValue = (this.previewData.setFieldsValues ? this.previewData.setFieldsValues[key] : ''),
				setFields = this.getSetFieldsTemplate(key, this.previewData.userFieldsList, setFieldValue),
				procTypes = this.getProcTypeTemplate(0, key, this.previewData.procFieldsList),
				lastClass = (this.previewData.importFieldsValues.length == i ? ' class="last-row"' : '');
			
			/** Построение строк */
			content += this.getRowTemplate(i, lastClass, valueField, setFields, procTypes, true);
		};
		if (!key) var key = 0;
		else key++;
		
		content += '<tr><td colspan="4"><button id="add-row" data-key="'+key+'">Добавить</button></td></tr>';
		
		$('#fields-wrap').remove();
		$('#preview', this.form).html('<div id="fields-wrap">'
			+ '<div class="title">Строка <input type="text" value="'+this.previewData.curString+'" id="string" onchange="prepareData.getImportFields($(this).val());" /> из '+this.previewData.rowsCount+''
			+ '<div class="options"><span id="save-config" class="pseudo-link">сохранить</span> / <span id="load-config" class="pseudo-link">загрузить</span></div></div>'
			+ '<table id="fields-table" cellspacing="0">'+content+'</table></div>'
		);					
		this.form.off('submit').on('submit', $.proxy(
			function(e){
				this.onBeforeSubmitForm(e);
			}, this)
		);
		$('input[type="submit"]', this.form).attr('disabled','disabled').val('Сохранить данные');
	},
	
	addField : function(key, value, copyKey)
	{
		var i = parseInt(key)+1,
			button = $('#add-row'),
			tr = button.closest('tr');
			
		button.attr('data-key', i);
		
		if (copyKey >= 0)
		{
			var isCopy = true,
				copyRowNumber = parseInt(copyKey)+1,
				copyValue = $('#fields-wrap tr[data-row="'+copyRowNumber+'"] .field .value').html();
		}
		
		var valueField = (
				isCopy
				? this.getValueFieldTemplate(key, copyValue, 'copy', copyKey)
				: this.getValueFieldTemplate(key, value, 'input')
			),
			setField = this.getSetFieldsTemplate(key, this.previewData.userFieldsList),
			procType = this.getProcTypeTemplate(0, key, this.previewData.procFieldsList);
		
		var newRow = this.getRowTemplate(i, '', valueField, setField, procType);
		
		tr.before(newRow);		
	},
	
	/**
	 * Возвращает html одной строки таблицы
	 */	
	getRowTemplate : function(rowNumber, rowClass, valueField, setField, procTypes, copy)
	{
		var key = parseInt(rowNumber)-1,
			addProcTypeTpl = this.getAddProcTypeTemplate(),
			copyTpl = (copy ? this.getCopyFieldTemplate(key) : '');
		
		return '<tr data-row="'+rowNumber+'"'+rowClass+'><td class="number">'+rowNumber+'. </td><td class="field">'+valueField+'<span class="processed"></span></td><td class="arrow"> => </td><td class="set-field">'+setField+'</td><td class="proc-field">'+procTypes+addProcTypeTpl+'</td><td class="meta">'+copyTpl+'<span class="status"></span></td></tr>';
	},
	
	getAddProcTypeTemplate : function()
	{
		return '<span class="add add-proc-type" title="Добавить">+</span>';
	}, 
	
	getCopyFieldTemplate : function(key)
	{
		return '<span class="copy" title="Копировать" data-copy="'+key+'">&crarr;</span>';
	}, 
	
	/**
	 * Возвращает html поля значения
	 */	
	getValueFieldTemplate : function(key, value, type, copyKey)
	{
		switch(type)
		{
			case 'input':
				var string = '<input type="text" class="value" name="addValues['+key+']" value="'+(value ? value : '')+'" />';
			break;
			case 'copy':
				var string = '<span class="value">'+value+'</span><input type="hidden" name="addValues['+key+']" value="COPY:'+copyKey+'" />';
			break;
			default: 
				var string = '<span class="value">'+value+'</span>';
			break;
		}
	
		return string;
	},
	
	/**
	 * Возвращает html поля назначения ключа
	 */	
	getSetFieldsTemplate : function(key, list, value)
	{
		var options = '';
		if (!list || (value && /^(UF_|PROPERTY_)/.test(value)))
		{
			return '<input type="text" class="link" value="'+(value ? value : '')+'" name="setFields['+key+']" />';
		}
		else 
		{
			for (var name in list) 
			{
				var selected = (name == value ? ' selected' : '');
				options += '<option value="'+name+'"'+selected+'>'+list[name]+'</option>';
			}
			return '<select class="link" name="setFields['+key+']">'+options+'</select>';
		}
	},

	/**
	 * Возвращает html поля вариантов обработки значения
	 */	
	getProcTypeListTemplate : function(procTypeId, key, list, value)
	{
		var options = '';
		for (var groupName in list)
		{
			options += '<optgroup label="'+groupName+'">'
			for (var name in list[groupName])
			{
				var selected = (name == value ? ' selected' : '');
				options += '<option value="'+name+'"'+selected+'>'+list[groupName][name]+'</option>';
			}
			options += '</optgroup>'
		}
		return '<select class="proc-type-list process" name="processingType['+key+']['+procTypeId+']">'+options+'</select>';
	},
	
	/**
	 * Возвращает html поля для обработки значения
	 */	
	getProcTypeTemplate : function(procTypeId, key, list, value, procData)
	{
		procData = procData || '';
		return '<div class="item" data-id="'+procTypeId+'">'+this.getProcTypeListTemplate(procTypeId, key, list, value)+procData+'</div>';
	},
	
	getProcDataFieldsGroup : function(procTypeId, key, row, itemKey, itemValue)
	{
		row = row || 0;
		itemKey = itemKey || '';
		itemValue = itemValue || '';
		
		return '<div class="proc-data-group" data-row="' + row + '">'
				+ '<input type="text" class="proc-data process filter-key" placeholder="доп. фильтр: ключ" data-name="filter" name="processingData['+key+']['+procTypeId+'][filter]['+row+'][key]" value="'+itemKey+'" />'
				+ '<input type="text" class="proc-data process filter-value" placeholder="значение" data-name="filter" name="processingData['+key+']['+procTypeId+'][filter]['+row+'][value]" value="'+itemValue+'" />'
				+ '<span class="add add-proc-data-group" title="Добавить">+</span>'
			+ '</div>';
	},		
	
	/**
	 * Сохраняем значения полей в php-сессии
	 */	
	savePreviewValues : function()
	{
		var data = new FormData(this.form[0]);
		data.append('isAjax', true);
		data.append('action', 'save_config');
		data.append('object', this.previewData.object);
		data.append('fieldsCount', this.previewData.fieldsCount);
		
		$.ajax({
			url: '',
			type: 'POST',
			data: data,
			contentType: false,
			processData: false,
			success: function(response){
				var json = JSON.parse(response);
				alert(json.MESSAGE);
			},			
			error: function(error){
				alert(error);
			},
		});	
	},
	
	/**
	 * Загружаем сохраненную конфигурацию в форму предпросмотра
	 */	
	loadPreviewValues : function()
	{		
		$.ajax({
			url: '',
			type: 'POST',
			data: { isAjax: true, action: 'load_config' },
			dataType: 'json',
			context: this,
			success: function(json)
			{
				if (json.SUCCESS)
				{
					var config = json.DATA,
						indexes = {};
					
					/** Если в сохраненной конфигурации было меньше полей, нужно пересчитать индексы */
					var diff = this.previewData.fieldsCount-json.FIELDS_COUNT;
					for (var i in config.setFields)
					{
						i = parseInt(i);
						var newIndex = ((diff > 0 && i > json.FIELDS_COUNT-1) ? i+diff : i);
						indexes[i] = newIndex;
					}
					
					/** Если есть доп. поля, создаем их и заполняем */
					for (var i in config.addValues)
					{
						if (/^COPY:\d+$/.test(config.addValues[i]))
							prepareData.addField(indexes[i], '', config.addValues[i].replace(/^COPY:(\d+)$/, '$1'));
						else
							prepareData.addField(indexes[i], config.addValues[i]);
					}
					
					/** Выводим поля для назначения ключей и заполняем их */
					for (var i in config.setFields)
					{						
						var list = (/^(UF|PROPERTY)_/.test(config.setFields[i]) ? '' : this.previewData.userFieldsList),
							input = this.getSetFieldsTemplate(indexes[i], list, config.setFields[i]),
							rowNumber = indexes[i]+1;						
						$('#fields-table tr[data-row="'+rowNumber+'"] .set-field').html(input);
						this.changeSetField(indexes[i]);
					}
					
					/** Выводим поля типов обработки */
					for (var i in config.processingType)
					{
						var rowNumber = indexes[i]+1,
							procFieldsHtml = '',
							addProcTypeHtml = this.getAddProcTypeTemplate();
						
						for (var procTypeId in config.processingType[i])
						{
							var procType = config.processingType[i][procTypeId],
								procData = (config.processingData && config.processingData[i] ? config.processingData[i][procTypeId] : {}),
								procDataHtml = this.getProcessingDataFields(procType, indexes[i], procTypeId, procData)
							
							procFieldsHtml += this.getProcTypeTemplate(procTypeId, indexes[i], this.previewData.procFieldsList, procType, procDataHtml);
						}
						$('#fields-table tr[data-row="'+rowNumber+'"] .proc-field').html(procFieldsHtml+addProcTypeHtml);
					}
					
					/** Обновляем предпросмотр обработанных по выбранному типу значений */
					$('#fields-table tr[data-row]').each(function(i){
						prepareData.processValue($(this).attr('data-row'));
					});
					
					/** Предупреждение */
					if (json.OBJECT !== this.previewData.object)
						alert('Объекты данных не совпадают: текущий - "'+this.previewData.object+'", сохраненный - "'+json.OBJECT+'"');
				}
				else alert(json.MESSAGE);
			},			
			error: function(error){
				console.log(error);
			},
		});	
	},
	
	onBeforeSubmitForm : function(e)
	{
	},
	
	/**
	 * Метод возвращает html доп. полей для типа обработки name
	 */	
	getProcessingDataFields : function(name, key, procTypeId, values)
	{
		var string = '',
			values = values || {};
		
		if (name == 'regexp')
		{
			var regexp = values['regexp'] || '',
				newstring = values['newstring'] || '';
				
			string = '<input type="text" class="proc-data process" placeholder="выражение" data-name="regexp" name="processingData['+key+']['+procTypeId+'][regexp]" value="'+regexp+'" /><input type="text" class="proc-data process" placeholder="замена" data-name="newstring" name="processingData['+key+']['+procTypeId+'][newstring]" value="'+newstring+'" />';
		}
		else if (name == 'explode' || name == 'implode')
		{
			var delimiter = values['delimiter'] || '';
			string = '<input type="text" class="proc-data process" placeholder="разделитель" data-name="delimiter" name="processingData['+key+']['+procTypeId+'][delimiter]" value="'+delimiter+'" />';
		}
		else if (name == 'array_item')
		{
			var itemKey = values['key'] || '';
			string = '<input type="text" class="proc-data process" placeholder="ключ" data-name="key" name="processingData['+key+']['+procTypeId+'][key]" value="'+itemKey+'" />';
		}
		else if (name == 'date')
		{
			var format = values['format'] || '';
			string = '<input type="text" class="proc-data process" placeholder="формат" data-name="format" name="processingData['+key+']['+procTypeId+'][format]" value="'+format+'" />';
		}
		else if (name == 'variants')
		{
			var types = { 'equal': 'равно', 'not_equal': 'не равно', 'preg_match': 'preg_match' },
				before = values['before'] || '',
				after = values['after'] || '';
				
			for (var k in types)
			{
				var selected = (values['type'] == k ? ' selected' : '');
				options += '<option value="'+k+'"'+selected+'>'+types[k]+'</option>';
			}
			
			string = '<input type="text" class="proc-data process" placeholder="значения ДО /" data-name="before" name="processingData['+key+']['+procTypeId+'][before]" value="'+before+'" />';
			string += '<select type="text" class="proc-data process" data-name="type" name="processingData['+key+']['+procTypeId+'][type]">'+options+'</select>';
			string += '<input type="text" class="proc-data process" placeholder="значения ПОСЛЕ /" data-name="after" name="processingData['+key+']['+procTypeId+'][after]" value="'+after+'" />';
		}
		else if (name == 'kladr_code_name' || name == 'kladr_name_code' || name == 'kladr_code_parent_code' || name == 'kladr_code_parent_name') 
		{
			var types = { 'region': 'Регион', 'district': 'Район', 'city': 'Нас. пункт', 'street': 'Улица', 'building': 'Здание' },
				options = optionsP = '';
			
			var i = 1;
			for (var k in types)
			{
				var selected = (values['kladr_type'] == k ? ' selected' : ''),
					selectedP = (values['kladr_parent_type'] == k ? ' selected' : '');
				options += '<option value="'+k+'"'+selected+'>'+types[k]+'</option>';
				if (i != Object.keys(types).length)
					optionsP += '<option value="'+k+'"'+selectedP+'>'+types[k]+'</option>';
				i++;
			}			
			string = '<select type="text" class="proc-data process" data-name="kladr_type" name="processingData['+key+']['+procTypeId+'][kladr_type]"><option value="">-- тип --</option>'+options+'</select>';
			
			if (name == 'kladr_code_parent_code' || name == 'kladr_code_parent_name')
				string += '<select type="text" class="proc-data process" data-name="kladr_parent_type" name="processingData['+key+']['+procTypeId+'][kladr_parent_type]"><option value="">-- тип родителя --</option>'+optionsP+'</select>';
		}
		else if (name == 'iblock' || name == 'user' || name == 'hl')
		{
			var block_id = values['block_id'] || '',
				param = values['param'] || '',
				filter = values['filter'] || [{ key: '', value: '' }],
				select = values['select'] || '';
				
			if (name == 'iblock' || name == 'hl')
				string = '<input type="text" class="proc-data process" placeholder="ID блока" data-name="block_id" name="processingData['+key+']['+procTypeId+'][block_id]" value="'+block_id+'" />';
			
			string += '<input type="text" class="proc-data process" placeholder="параметр фильтра" data-name="param" name="processingData['+key+']['+procTypeId+'][param]" value="'+param+'" />';

			for (var i in filter)
				string += prepareData.getProcDataFieldsGroup(procTypeId, key, i, filter[i].key, filter[i].value);
			
			string += '<input type="text" class="proc-data process" placeholder="что получить " data-name="select" name="processingData['+key+']['+procTypeId+'][select]" value="'+select+'" />';
		}
		
		return string;
	},	
	
	changeProcessingType : function(rowNumber, procTypeId)
	{		
		var tr = $('#fields-wrap tr[data-row="'+rowNumber+'"]'),
			parent = tr.find('.proc-field [data-id="'+procTypeId+'"]'),
			$this = $('.proc-type-list', parent),
			key = parseInt(rowNumber)-1,
			procType = $this.val();
		
		$('.proc-data', parent).remove()
		$('.processed', tr).text('');
		
		var dataFields = prepareData.getProcessingDataFields(procType, key, procTypeId);
		
		if (dataFields)
		{
			$this.after(dataFields);
		}
		else
		{			
			prepareData.processValue(rowNumber);
		}			
	},
	
	processValue : function(rowNumber)
	{
		var formData = new FormData(this.form[0]),
			tr = $('#fields-wrap tr[data-row="'+rowNumber+'"]'),
			valueElem = $('.value', tr),
			processedElem = $('.processed', tr),
			value = (
				valueElem.attr('type') == 'text'
				? valueElem.val() 
				: valueElem.text()
			);
		
		var ajaxData = new FormData(),
			keyType = 'processingType[' + (rowNumber - 1) + ']',
			keyData = 'processingData[' + (rowNumber - 1) + ']';
			
		if (formData.get(keyType + '[0]'))
		{
			for (var [key, val] of formData.entries())
			{
				if (key.indexOf(keyType) === 0)
				{
					ajaxData.append(key, val);
				}
				else if (key.indexOf(keyData) === 0)
				{
					ajaxData.append(key, val);
				}
			}
			
			ajaxData.append('isAjax', true);
			ajaxData.append('action', 'processValue');
			ajaxData.append('value', value);
			
			$.ajax({
				url: '',
				type: 'POST',
				data: ajaxData,
				contentType: false,
				processData: false,
				success: function(json)
				{
					var result = JSON.parse(json);
					if (result.SUCCESS) 
						processedElem.text(' / '+result.VALUE+'');
				},			
				error: function(error){
					alert(error);
				},
			});
		}
			
	},
	
	/**
	 * Проверка взаимосвязей значение-ключ для всех полей во избежание дублирования ключей
	 */	
	changeSetField : function(rowNumber)
	{
		var disabled = true,
			key = parseInt(rowNumber)-1,
			$this = $('#fields-wrap tr[data-row="'+rowNumber+'"] .link');
			
		var value = $this.val(),
			tr = $this.closest('tr'),
			form = $this.closest('form'),
			submit = form.find('input[type="submit"]');
			
		// Заменяем список полем ввода
		if (value == 'INPUT' || value == 'PROPERTY' || value == 'UF')
		{
			var inputValue = (value == 'INPUT' ? '' : value+'_'),
				input = prepareData.getSetFieldsTemplate(key, '', inputValue);
			$this.after(input).remove();
			$this = tr.find('.link');
		}
		
		// Каждый раз включаем все опции и заново формируем массив занятых полей
		var used = [];
		$('.link option', form).removeAttr('disabled');	
		// Статус поля
		if (value) 
			tr.find('.status').addClass('ready').html('&#10003;');
		else 
			tr.find('.status').removeClass('ready').html('');		
		// Формирование массива занятых полей и включение кнопки
		form.find('.link').each(
			function(i,item)
			{
				if ($(item).val().length != 0)
				{
					disabled = false;
					used.push($(item).val());
				}
			}
		);
		// Для каждого занятого поля проходимся по всем селектам, кроме его занимающих, и отключаем у них опции выбора занятого поля
		for (var key in used)
		{
			form.find('.link').each(
				function(i,item)
				{
					if ($(item).val() != used[key]) $('option[value="'+used[key]+'"]', $(item)).attr('disabled','disabled');
				}
			);				
		}
		// Статус кнопки в зависимости от заполненности всех селектов
		submit.attr('disabled',disabled);
	},

	
};