$(function(){
	
	formState.init($('.show-depend'));	
			
	/** Добавление нового поля */	
	$('.add-field').on('click', function() {
		var parent = $(this).parent(),
			lastItem = $('.item', parent).last(),
			lastItemId = lastItem.attr('data-id'),
			newItemId = parseInt(lastItemId)+1;
		
		var newItem = lastItem.clone();
		newItem.attr('data-id', newItemId).find('input,select').val('');
		
		var replaceAttrs = ['name','id'];
		
		$('select,input', newItem).each(function() {
			for(var i in this.attributes) { 
				if(replaceAttrs.indexOf(this.attributes[i].name) !== -1) {
					this.attributes[i].value = this.attributes[i].value.replace(lastItemId, newItemId);
				}
			}
		});
		lastItem.after(newItem);
	});
	
	/** Сохранение формы */	
	$('#save-form').on('click', function(){
		saveForm($(this));
	});
  
  /** Preset */
  $('.row').on('change', '.preset-field', function(){
    var currentOption = $('[value="' + $(this).val() + '"]', this); 
    if (!currentOption)
      return;
    
    var items = ['name', 'value'];
    for (var i in items) {
      var name = items[i];
      var value = currentOption.data(name) || '';
      var selector = '[name="' + $(this).attr('name').replace('presetId', name) + '"]';
      
      $(selector).val(value);
    }    
  });	
	
});

function copyToClipboard(text) {
	window.prompt('Нажмите Ctrl+C, чтобы скопировать выделенный текст', text);
}
  
function saveForm(elem)
{
	var form = $('form.main-form'),
		data = new FormData(form);
	data.append('isAjax', true);
	data.append('action', 'save_form');
	
	$.ajax({
		url: form.attr('action'),
		type: 'POST',
		data: data,
		contentType: false,
		processData: false,
		success: function(response){
			var json = JSON.parse(response);
		},			
		error: function(error){
			alert(error);
		},
	});	
}

function jsonPrint(data)
{
	return '<pre>'+syntaxHighlight(data,null,4)+'</pre>';
}

function syntaxHighlight(json) {
	if (typeof json != 'string') {
		json = JSON.stringify(json, undefined, 4);
	}
	json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
		var cls = 'number';
		if (/^"/.test(match)) {
			if (/:$/.test(match)) {
				cls = 'key';
			} else {
				cls = 'string';
			}
		} else if (/true|false/.test(match)) {
			cls = 'boolean';
		} else if (/null/.test(match)) {
			cls = 'null';
		}
		return '<span class="' + cls + '">' + match + '</span>';
	});
}