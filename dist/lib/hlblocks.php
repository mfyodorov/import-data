<?
namespace BitrixData;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException as Exc;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class HLBlocks 
{	
	/**
	 * Метод проверяет подключение необходимых модулей
	 * @throws LoaderException
	 */
	protected static function checkModules()
	{
		if(!Loader::includeModule('highloadblock'))
			throw new Exc('Module highloadblock is not installed');
	}
	
	/**
	 * Метод строит запрос на получение записей highload блока и выполняет его
	 *
	 * @param int $blockId - идентификатор highload-блока
	 * @param array $params - массив параметров запроса
	 * @return object|bool
	 */	
	public function execQueryHL($blockId, $params = array())
	{
		self::checkModules();
		
		$blockId = intval($blockId);
		
		if(!$blockId)
			return false;
		
		$hlblock = HL\HighloadBlockTable::getById($blockId)->fetch();
		
		if(!empty($hlblock))
        {
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);			
			$q = new Entity\Query($entity);
			
			$arSelect = (is_array($params['select']) ? $params['select'] : array('*'));			
			$q->setSelect($arSelect);
			
			if(is_array($params['filter']))
				$q->setFilter($params['filter']);
			if(is_array($params['group']))
				$q->setGroup($params['group']);			
			if(is_array($params['order']))
				$q->setOrder($params['order']);			
			if(intval($params['limit']))
				$q->setLimit($params['limit']);
			if(intval($params['offset']))
				$q->setOffset($params['offset']);
			if($params['count_total'])
				$q->countTotal(true);
			
			$result = $q->exec();
			
			return $result;
		}
		
		return false;		
	}
	
	/**
	 * Метод для получения списка записей из highload-блока
	 * Метод-обертка для метода execQueryHL()
	 *
	 * @param $blockId - идентификатор highload-блока
	 * @param $params - массив параметров запроса
	 * @param $keys - если true, назначит в качестве ключей выходного массива ID записей, если строка - поля по этой строке (например XML_ID)
	 * @return array|bool
	 */	
	public function getRows($blockId, $params = array(), $keys = false)
	{
		$blockId = intval($blockId);
		
		if(!$blockId)
			return false;
		
		$result = self::execQueryHL($blockId, $params);
		
		if(!empty($result))
        {			
			$array = array();
			
			while($row = $result->fetch())
			{
				if($keys === true) $array[$row['ID']] = $row;
				elseif(strlen($keys) > 0) $array[$row[$keys]] = $row;
				else $array[] = $row;
			}
			return $array;			
		}
		
		return false;		
	}	
	
	/**
	 * Метод для добавления записи в highload-блок
	 *
	 * @param $blockID - идентификатор highload-блока
	 * @param $arData - массив полей
	 * @return array|bool
	 */	
	public function addRow($blockID,$arData = array())
	{
		self::checkModules();
		
		if(!intval($blockID) || empty($arData))
			return false;
		
		$hlblock = HL\HighloadBlockTable::getById($blockID)->fetch();
		
		if(!empty($hlblock))
        {
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);					
			$edc = $entity->getDataClass();			
			$result = $edc::add($arData);
			
			if($result->isSuccess())
			{
				return $result->getId();
			}  
			else 
			{
				return $result->getErrorMessages();
			} 		
		}
		
		return false;		
	}
		
	/**
	 * Метод для обновления записи в highload-блоке
	 *
	 * @param $blockID - идентификатор highload-блока
	 * @param $ID - идентификатор записи
	 * @param $arData - массив полей
	 * @return array|bool
	 */	
	public function updateRow($blockID,$ID,$arData = array())
	{
		self::checkModules();
		
		if(!intval($blockID) || !intval($ID) || empty($arData))
			return false;
		
		$hlblock = HL\HighloadBlockTable::getById($blockID)->fetch();
		
		if(!empty($hlblock))
        {
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);					
			$edc = $entity->getDataClass();			
			$result = $edc::update($ID,$arData);
			
			if($result->isSuccess())
			{
				return true;
			}  
			else 
			{
				return $result->getErrorMessages();
			} 		
		}
		
		return false;		
	}	
}
