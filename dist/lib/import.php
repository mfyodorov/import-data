<?
namespace BitrixData;

use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Web\Json;

class Import
{
	public static function getLogFilePath()
	{
		return Main::getDirPath().'/data/__webservice.log';
	}
	
	public static function toWebservice($arItem, $arParams, &$ACTION, &$count, $IN)
	{
		$authData = (
			$arParams['auth']
			? array('login' => $arParams['login'], 'password' => $arParams['password']) 
			: array()
		);
		
		if ($arParams['php_code_before'])
			$ACTION['PHP_CODE']['before'][] = self::runPhpCode($arParams['php_code_before'], $arItem);
			
		$data = $arItem;
			
		if ($arParams['protocol'] == 'SOAP')
		{
			$result = Main::getSoapResponse($arParams['method'], $arParams['url'], $authData, $data);
			
			$key = ($arItem['ID'] ? $arItem['ID'] : '0'.($IN+$count));
			
			if ($arParams['php_code_after'])
				$ACTION['PHP_CODE']['after'][] = self::runPhpCode($arParams['php_code_after'], $arItem);
			
			$ACTION['RESULT'][$key] = $result;
			$ACTION['IDS'] .= $key.', ';
			$ACTION['COUNT'] = (!isset($ACTION['COUNT']) ? 1 : $ACTION['COUNT']+1);
			
			if ($result['ERROR'] && $result['TYPE'] == 'exception')
			{
				$ACTION['RESULT'][$key]['DATA'] = $arItem;
				Debug::writeToFile($arItem, 'Array', self::getLogFilePath());
				Debug::writeToFile($data, 'Data', self::getLogFilePath());
				Debug::writeToFile($result['MESSAGE'], 'Exception', self::getLogFilePath());
				
				return false;
			}
		}
		elseif (in_array($arParams['protocol'], ['POST', 'GET']))
		{
			foreach ($arParams['headers'] as $item)
				if ($item['name'])
					$headers[$item['name']] = $item['value'];
				
			if ($arParams['auth'] == 'basic')
				$headers['Authorization'] = 'Basic ' . base64_encode($authData['login'] . ':' . $authData['password']);
			
			$result = (
				$arParams['protocol'] === 'POST'
				? Main::getHttpPostResponse($arParams['url'], $data, $headers)
				: Main::getHttpGetResponse($arParams['url'], $data, $headers)
			);
			
			if ($arParams['decode_json'] == 'Y')
				$result = json_decode($result, true);
			
			$key = ($arItem['ID'] ? $arItem['ID'] : '0'.($IN+$count));
			
			if ($arParams['php_code_after'])
				$ACTION['PHP_CODE']['after'][] = self::runPhpCode($arParams['php_code_after'], $arItem);
			
			$ACTION['RESULT'][$key] = $result;
			$ACTION['IDS'] .= $key.', ';
			$ACTION['COUNT'] = (!isset($ACTION['COUNT']) ? 1 : $ACTION['COUNT']+1);
			
			if ($result['ERROR'] && $result['TYPE'] == 'exception')
			{
				$ACTION['RESULT'][$key]['DATA'] = $arItem;
				Debug::writeToFile($arItem, 'Array', self::getLogFilePath());
				Debug::writeToFile($data, 'Data', self::getLogFilePath());
				Debug::writeToFile($result['MESSAGE'], 'Exception', self::getLogFilePath());
				
				return false;
			}
		}
		
		++$count;

		return true;
	}		
	
	/**
	 * Метод обрабатывает переданный массив элементов.
	 * Если параметр action = ADD, переданные элементы создаются в базе сайта.
	 * Если параметр action = UPDATE, производится поиск переданных элементов и сравнение их полей со значениями в базе, 
	 * а так же производится обновление полей элементов.
	 *
	 * @param array $arItem - массив элементов для импорта
	 * @param array $arParams - массив параметров импорта
	 * @param array &$ACTION - изменяемый массив, содержащий статистику выполненных действий
	 * @param int &$count - кол-во обработанных элементов
	 * @param int $IN - кол-во элементов всех элементов, пройденных перед данным шагом
	 */
	public static function toBitrix($arItem, $arParams, &$ACTION, &$count, $IN)
	{
		if ($arParams['php_code_before'])
			$ACTION['PHP_CODE']['before'][] = self::runPhpCode($arParams['php_code_before'], $arItem);
		
		/** Добавление новых сущностей */
		if ($arParams['action'] == 'ADD')
		{
			$actionResult = array('TYPE' => $arParams['action']);
			
			if ($arParams['object'] == 'USER' || $arParams['object'] == 'ELEMENT')
			{
				if ($arParams['object'] == 'USER')
				{
					if ($arParams['login'] == 'Y')
						$fields['LOGIN'] = $arItem['EMAIL'];
					if ($arParams['generate_pass'] == 'Y')
						$fields['PASSWORD'] = $fields['CONFIRM_PASSWORD'] = self::generatePassword();
				}
				else $fields['IBLOCK_ID'] = $arParams['iblock_id'];
				
				foreach ($arItem as $key => $item)
				{
					if (strpos($key, 'PROPERTY_') !== false)
					{
						$key = preg_replace('#^PROPERTY_(.+)$#', "$1", $key);
						
						$arItem = (!is_array($item) ? array($item) : $item);
						
						foreach ($arItem as $i => $val)
						{
							if (preg_match('#\.(jpg|jpeg|png|gif)$#i', $val))
							{
								$arItem['n'.$i] = array('VALUE' => \CFile::MakeFileArray($val), 'DESCRIPTION' => '');
								unset($arItem[$i]);
							}
							else
							{
								/** Если в строке встречается ///, значит после этого разделителя идет описание свойства */	
								$arValue = explode('///', $val);
								$value = trim($arValue[0]);
								$desc = (trim($arValue[1]) ? trim($arValue[1]) : '');
								$arItem[$i] = array('VALUE' => $value, 'DESCRIPTION' => $desc);
							}							
						}
						
						$fields['PROPERTY_VALUES'][$key] = (is_array($item) ? $arItem : $arItem[0]);
					}
					else
					{
						if ($key == 'ACTIVE')
						{
							if ($item == 'Да') $item = 'Y';
							elseif ($item == 'Нет') $item = 'N';
						}
						elseif ($key == 'PREVIEW_PICTURE' || $key == 'DETAIL_PICTURE')
							$item = \CFile::MakeFileArray($item);
						elseif ($key == 'GROUP_ID')
							$item = explode(',', $item);
						
						$fields[$key] = $item;
					}
				}
				$add = ($arParams['object'] == 'USER' ? new \CUser : new \CIBlockElement);								
				$addResult = $add->Add($fields);
				if ($addResult)
				{											
					$actionResult['SUCCESS'] = true;
					$actionResult['ID'] = $addResult;
				}
				else
					$actionResult['ERROR'] .= $add->LAST_ERROR;
				
			}
			elseif ($arParams['object'] == 'HL')
			{
				try
				{
					$fields = $arItem;							
					$addResult = HLBlocks::addRow($arParams['iblock_id'], $fields);
					if (!$addResult)
						$actionResult['ERROR'] = 'Wrong parameters for addRow()';
					elseif (is_array($addResult))
						$actionResult['ERROR'] = implode('; ',$addResult);
					else
					{
						$actionResult['SUCCESS'] = true;
						$actionResult['ID'] = $addResult;
					}
				}
				catch(\Throwable $e)
				{
					$actionResult['ERROR'] = $e->getMessage();
				}						
			}
		}
		/** Обновление сущностей */
		elseif ($arParams['action'] == 'UPD')
		{
			if (!empty($arItem[$arParams['filter_by']]))
			{
				
				/** Для пользователей не работает жесткое сравнение с помощью '=' */
				$filterType = ($arParams['object'] == 'ELEMENT' || $arParams['object'] == 'HL' ? '=' : '');
				$arFilter = array($filterType.$arParams['filter_by'] => $arItem[$arParams['filter_by']]);		
				
				/**
				 * Подготавливаем объект запроса и выполняем запрос на получение по фильтруемому параметру
				 * всех записей для текущего типа объекта (пользователи, элементы инфоблока, записи highload блока)
				 */	
				switch ($arParams['object'])
				{
					case 'USER':
						$arSelect = array();
						foreach (array_keys($arItem) as $field)
						{
							if (preg_match('#^UF_#', $field)) $arSelect['SELECT'][] = $field;
							else $arSelect['FIELDS'][] = $field;
						}
						$res = \CUser::GetList(($by = "id"), ($order = "desc"), $arFilter, $arSelect);
					break;
					case 'ELEMENT':
						$arSelect = array_merge(array("ID", "IBLOCK_ID", "NAME"), array_keys($arItem));
						$arFilter["IBLOCK_ID"] = $arParams['iblock_id'];
						$res = \CIBlockElement::GetList(array("sort" => "asc"), $arFilter, false, false, $arSelect);							
					break;
					case 'HL':
						try
						{	
							$res = HLBlocks::execQueryHL(
								$arParams['iblock_id'], 
								array(
									'select' => array_merge(array('ID'), array_keys($arItem)),
									'filter' => $arFilter
								)
							);
							$res = (!$res ?: new \CDBResult($res));
						}
						catch(\Throwable $e)
						{
							$actionResult['ERROR'] = $e->getMessage();
						}	
					break;
					case 'TICKET':
						$by = 's_id'; $order = 'asc';
						if ($arParams['filter_by'] == 'ID') 
							$arFilter['ID_EXACT_MATCH'] = 'Y';
						$arSelect = array('SELECT' => array('UF_*'));
						$res = \CTicket::GetList($by, $order, $arFilter, $isFiltered, 'N', 'N', 'N', false, $arSelect);
					break;
					case 'UF':
						$res = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields($arParams['entity_id'], $arItem[$arParams['filter_by']]);
					break;
				}
				
				if ($res)
				{
					$ob = (
						is_array($res)
						? $res
						: $res->GetNext(false, false)
					);
					
					if ($ob)
					{
						/** Устанавливаем первичный ключ для последующего обновления */
						$primary = (
							$arParams['object'] === 'UF'
							? $arItem[$arParams['filter_by']]
							: $ob['ID']
						);
						
						/**
						 * Проверка всех полей кроме фильтруемого на совпадение с реальными значениями в базе
						 * Внутри блока foreach значение каждого поля импортированных данных (кроме фильтруемого и ID)
						 * сравнивается со значением параметра с таким же ключом, полученным из базы
						 * Для более точного сравнения для некоторых полей (определяются по ключам) производится настройка.
						 * Так же в данном блоке подготавливаются значения массива $arItem и создается массив $arProps, 
						 * которые потом будут использоваться для обновления данных
						 */ 
						$difference = false; // отличия в значениях полей отсутсвуют
						$arProps = array();
						foreach ($arItem as $key => $value)
						{
							if ($key == $arParams['filter_by']) 
								continue;
							
							/** Значение поля из базы данных перед обновлением */
							$dbValue = (
								$arParams['object'] === 'UF'
								? $ob[$key]['VALUE']
								: $ob[$key]
							);
							
							/**
							 * Если это свойство инфоблока, удаляем его из массива полей $arItem и добавляем в массив свойств $arProps
							 * Так же изменяем ключ для сравнения (добавляем постфикс _VALUE, т.к. из базы приходят ключи такого вида)
							 */	
							$pos = strpos($key, 'PROPERTY_');
							if ($pos !== false)
							{
								/**
								 * Если в строке встречается ///, значит после этого разделителя идет описание свойства
								 */	
								$arValue = explode('///', $value);
								$value = trim($arValue[0]);
								$desc = (trim($arValue[1]) ? trim($arValue[1]) : '');
								
								if ($arParams['update'] == "Y")
								{	
									if (!empty($value) || $arParams['empty'] == "Y")
									{									
										$propID = preg_replace('#^PROPERTY_#','',$key);										
										$arProps[$propID] = array('VALUE' => $value, 'DESCRIPTION' => $desc);
									}						
									unset($arItem[$key]);
								}							
								$key = strtoupper($key).'_VALUE';
							}
							
							/**
							 * Для значений email сравниваем независимо от регистра
							 */	
							if ($key == "EMAIL")
							{
								$dbValue = strtolower($dbValue);
								$value = strtolower($value);
							}
							/**
							 * Если это значение для добавления в группы пользователей, получаем существующие группы доп. запросом
							 * Сравниваем значения прямо тут и выходим из цикла, т.к. в овновном запросе поле с группами не приходит
							 * Подготавливаем значение в $arItem (должен быть массив) для последующего обновления
							 */	
							if ($key == "GROUP_ID")
							{
								$arGroups = $arGroupsArray = array();
								$res = \CUser::GetUserGroupList($primary);
								while($arGroup = $res->Fetch())
								{
									$arGroups[] = $arGroup['GROUP_ID']; // массив идентификаторов групп
									$arGroupsArray[] = $arGroup; // массив с информацией о группах, включая даты активности
								}
								$arItem[$key] = $arGroupsArray;
								$value = explode('+',$value);
								foreach ($value as $groupID)
								{
									if (!in_array($groupID, $arGroups))
									{
										$difference = true; // есть хотя бы одно отличие в значениях полей
										$_SESSION['MISHA']['STAT']['NOT_MATCH_'.$key][] = sprintf(
											'(%s: %s) запрос: (%s), реально: (%s)',
											$arParams['filter_by'],
											$arItem[$arParams['filter_by']],
											$groupID,
											implode(',',$arGroups)											
										);
										$arItem[$key][] = array('GROUP_ID' => $groupID);
									}
								}
								continue;
							}
							
							/** Сравниваем значение из базы с переданным значением */
							if ($dbValue != $value)
							{
								$difference = true; // есть хотя бы одно отличие в значениях полей
								$_SESSION['MISHA']['STAT']['NOT_MATCH_'.$key][] = sprintf(
									'(%s: %s) запрос: (%s), реально: (%s)',
									$arParams['filter_by'],
									$arItem[$arParams['filter_by']],
									$value,
									$dbValue
								);
							}
						}					
						
						/**
						 * Обновление полей
						 */	
						if ($arParams['update'] == "Y")
						{
							/**
							 * Если значения всех полей не отличаются от значений в БД, не производим обновление
							 */	
							if (!$difference)	
							{
								$ACTION['MISS']['COUNT'] = (!isset($ACTION['MISS']['COUNT']) ? 1 : $ACTION['MISS']['COUNT']+1);
								$ACTION['MISS']['IDS'] .= $primary.', ';
								++$count;
								return true;
							}
							
							$actionResult = array('TYPE' => $arParams['action']);							
							
							$arFields = $arItem;
							unset($arFields[$arParams['filter_by']],$arFields["ID"]);
							
							/**
							 * Если есть поля для обновления, обновляем их основным методом Update
							 */	
							if (!empty($arFields))
							{
								if ($arParams['object'] == 'USER' || $arParams['object'] == 'ELEMENT')
								{
									$upd = ($arParams['object'] == 'USER' ? new \CUser : new \CIBlockElement);								
									$updResult = $upd->Update($primary, $arFields);
									if ($updResult)
									{
										$actionResult['SUCCESS'] = true;
										$actionResult['ID'] = $primary;
									}
									else $actionResult['ERROR'] .= $upd->LAST_ERROR."\n";								
								}
								elseif ($arParams['object'] == 'HL')
								{						
									try
									{
										$updResult = HLBlocks::updateRow($arParams['iblock_id'], $primary, $arFields);
										if (!$updResult)
											$actionResult['ERROR'] .= 'Wrong parameters for updateRow()'."\n";
										elseif (is_array($updResult))
											$actionResult['ERROR'] .= implode('; ',$updResult)."\n";
										else
										{
											$actionResult['SUCCESS'] = true;
											$actionResult['ID'] = $primary;
										}
									}
									catch(\Throwable $e)
									{
										$actionResult['ERROR'] .= $e->getMessage()."\n";
									}								
								}
								elseif ($arParams['object'] == 'TICKET')
								{
									$updResult = \CTicket::Set($arFields, $messageId, $primary, 'N', 'N', 'N');
									$updResult = true;
									if ($updResult)
									{
										$actionResult['SUCCESS'] = true;
										$actionResult['ID'] = $primary;
									}
									else $actionResult['ERROR'] .= '\CTicket::Set error'."\n";
								}	
								elseif ($arParams['object'] == 'UF')
								{
									$updResult = $GLOBALS['USER_FIELD_MANAGER']->Update($arParams['entity_id'], $primary, $arFields);	
									$updResult = true;
									if ($updResult)
									{
										$actionResult['SUCCESS'] = true;
										$actionResult['ID'] = $primary;
									}
									else $actionResult['ERROR'] .= "\$GLOBALS['USER_FIELD_MANAGER']->Update error\n";
								}
							}
							
							/**
							 * Если есть свойства для обновления элемента инфоблока, обновляем их дополнительным методом
							 */	
							if ($arParams['object'] == 'ELEMENT' && !empty($arProps))
							{								
								\CIBlockElement::SetPropertyValuesEx($primary, $arParams['iblock_id'], $arProps);
								$actionResult['SUCCESS'] = true;
								$actionResult['ID'] = $primary;
							}
						}
						
						++$count;
					}
				}
				else
				{				
					// Элемент не найден по искомому параметру $arParams['filter_by']
					$_SESSION['MISHA']['STAT']['NOT_FOUND_BY_'.$arParams['filter_by']][] = $arItem[$arParams['filter_by']];	
				}
			}
		}
		
		if ($arParams['php_code_after'])
			$ACTION['PHP_CODE']['after'][] = self::runPhpCode($arParams['php_code_after'], $arItem);
		
		/**
		 * Если требовалось проиводить действие, возвращается массив с результатом
		 * Обрабатываем его и записываем в сессию
		 */
		if (isset($actionResult))
		{
			if ($actionResult['SUCCESS'])
			{
				$ACTION[$actionResult['TYPE']]['COUNT'] = (!isset($ACTION[$actionResult['TYPE']]['COUNT']) ? 1 : $ACTION[$actionResult['TYPE']]['COUNT']+1);
				$ACTION[$actionResult['TYPE']]['IDS'] .= $actionResult['ID'].', ';
			}
			else
			{
				$ACTION['ERROR'][] = $actionResult['TYPE'].' "'.reset($array).'" Error: '.$actionResult['ERROR'];	
			}	
			unset($actionResult);
		}
		
		return true;
	}
	
	public static function runPhpCode($query, &$item)
	{
		ini_set('html_errors', 0);
	
		ob_start();
		
		$query = rtrim($query, ";\x20\n").";\n";
		
		$stime = microtime(1);
		
		try 
		{
			eval($query);
			$result = ob_get_contents();
		}
		catch (\Throwable $e)
		{
			$result = sprintf('Exception: %s', $e->getMessage());
		}
		
		$result = sprintf(
			'%s(time: %0.6f)', 
			($result ? $result . ' ' : ''),
			microtime(1) - $stime
		);
		
		ob_end_clean();
		
		return $result;
	}
	
	/**
	 * Метод для генерации произвольной строки
	 * @param int $length - длина строки
	 * @return string
	 */
	public static function generatePassword($length)
	{
		if (!intval($length))
			$length = 10;
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
		$password = substr( str_shuffle( $chars ), 0, intval($length) );
		return $password;
	}
}