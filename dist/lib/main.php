<?
namespace BitrixData;

use Bitrix\Main\Application,
	Bitrix\Main\Diag\Debug,	
	Bitrix\Main\IO,
	Bitrix\Main\Loader,
	Bitrix\Main\Web\HttpClient;

class Main
{
	public static function getFormFilePath($form)
	{
		return self::getDataDirPath() . DIRECTORY_SEPARATOR . 'form_'.$form.'.json';
	}
	
	public static function getDataDirPath()
	{
		$path = self::getDirPath(true) . DIRECTORY_SEPARATOR . 'data';
		
		if (!IO\Directory::isDirectoryExists($path))
			IO\Directory::createDirectory($path);
		
		return $path;
	}
	
	public function getData($filePath = BD_DATA_FILE)
	{
		$json = self::getFileContents($filePath);
		if ($json !== false)
			return json_decode($json, true);
		return false;
	}
	
	public function setData($data, $filePath = BD_DATA_FILE)
	{
		$json = (is_array($data) ? json_encode($data) : $data);	
		return self::putFileContents($filePath, $json, LOCK_EX);
	}

	public static function getFormValues($name)
	{	
		$data = array();
		$json = self::getFileContents(self::getFormFilePath($name));					
		if($json)
			$data = json_decode($json, true);
		return $data;
	}
	
	public static function saveFormValues($name, $data)
	{	
		$json = json_encode($data);
		$path = self::getFormFilePath($name);
		$res = self::putFileContents($path, $json, LOCK_EX);
		return $res;
	}
	
	public function checkParams($page, $params)
	{
		switch($page)
		{
			case 'prepare': 
				if($params['source'] == 'bitrix' && $params['bitrix_entity'] == 'ELEMENT'  && !Loader::includeModule('iblock'))
					return array('SUCCESS' => true, 'MESSAGE' => 'Iblock module is not installed');
				
				if($params['source'] == 'bitrix' && $params['bitrix_entity'] == 'HL'  && !Loader::includeModule('highloadblock'))
					return array('SUCCESS' => true, 'MESSAGE' => 'Highloadblock module is not installed');
				
				if($params['source'] == 'bitrix' && $params['bitrix_entity'] == 'TICKET'  && !Loader::includeModule('support'))
					return array('SUCCESS' => true, 'MESSAGE' => 'Support module is not installed');
				
				if($params['source'] == 'bitrix' && $params['bitrix_entity'] == 'STAT_EVENT'  && !Loader::includeModule('statistic'))
					return array('SUCCESS' => true, 'MESSAGE' => 'Statistic module is not installed');
			break;
			case 'import_bitrix': 
				if($params['object'] == 'ELEMENT' && !Loader::includeModule('iblock'))
					return array('SUCCESS' => true, 'MESSAGE' => 'Iblock module is not installed');

				if($params['object'] == 'HL' && !Loader::includeModule('highloadblock'))
					return array('SUCCESS' => true, 'MESSAGE' => 'Highloadblock module is not installed');

				if($params['object'] == 'TICKET' && !Loader::includeModule('support'))
					return array('SUCCESS' => true, 'MESSAGE' => 'Support module is not installed');
			break;
			case 'import_webservice': 
				if($params['protocol'] == 'SOAP' && !Loader::includeModule('webservice'))
					return array('SUCCESS' => true, 'MESSAGE' => 'Webservice module is not installed');
			break;			
		}
		
		return array('SUCCESS' => true);
	}
	
	/**
	 * Метод класса, обрабатывающий ajax запросы
	 * @return array
	 */
	public static function ajax()
	{	
		if($_POST['isAjax'])
		{
			global $APPLICATION;
			$APPLICATION->RestartBuffer();
			
			$result = array('SUCCESS' => false, 'MESSAGE' => 'Заполните обязательные поля');
			
			switch($_POST['action'])
			{
				case 'save_config':
					$config = array(
						'addValues' => $_POST['addValues'],
						'setFields' => $_POST['setFields'],
						'processingType' => $_POST['processingType'],
						'processingData' => $_POST['processingData'],
					);
					$json = json_encode(array('CONFIG' => $config, 'OBJECT' => $_POST['object']));
					$res = self::putFileContents(self::getConfigFilePath(), $json, LOCK_EX);
					if($res === false)
						$result = array('SUCCESS' => false, 'MESSAGE' => 'Ошибка сохранения конфигурации');
					else
						$result = array('SUCCESS' => true, 'MESSAGE' => 'Конфигурация сохранена');
				break;
				case 'load_config':
					$result = array('SUCCESS' => false, 'MESSAGE' => 'Нет сохраненной конфигурации');
					$json = self::getFileContents(self::getConfigFilePath());					
					if($json)
					{
						$data = json_decode($json, true);
						if(is_array($data))
							$result = array(
								'SUCCESS' => true, 
								'DATA' => $data['CONFIG'], 
								'OBJECT' => $data['OBJECT']
							);
					}
				break;
			}
			
			die(json_encode($result));		
		}
	}
	
	/**
	 * Метод возвращает путь к корневой папке функционала
	 * @param boolean $docRoot
	 * @return string
	 */
	public static function getDirPath($docRoot = false)
	{
		$curDir = preg_replace('#.{1}lib$#', '', __DIR__);
		return ($docRoot ? $curDir : str_replace(Application::getDocumentRoot(), '', $curDir));
	}
	
	/**
	 * Метод возвращает массив возможных типов преобразования массива даных в строку
	 * @return array
	 */
	public static function getStringTypes()
	{
		$array = array(
			'' => '-- выберите тип --',
			'JSON' => 'JSON',
			'XML' => 'XML'
		);
		return $array;
	}
	
	/**
	 * Метод возвращает массив типов запроса
	 * @return array
	 */
	public static function getRequestTypes()
	{
		$array = array(
			'SOAP' => 'SOAP',
			'GET' => 'HTTP (GET)',
			'POST' => 'HTTP (POST)'
		);
		return $array;
	}
	
	/**
	 * Метод возвращает массив доступных типов авторизации
	 * @return array
	 */
	public static function getAuthTypes()
	{
		$array = array(
			'' => '-- нет --',
			'basic' => 'Basic'
		);
		return $array;
	}
	
	/**
	 * Метод возвращает массив заголовков
	 * @return array
	 */
	public static function getHttpHeaders()
	{
		$array = array(
			'' => array('label' => '-- пресет --'),
			'json' => array('name' => 'Content-Type', 'value' => 'application/json'),
		);
		return $array;
	}
	
	/**
	 * Метод возвращает массив доступных методов обработки импортируемых полей
	 * @return array
	 */
	public static function getObjects()
	{
		$array = array(
			'USER' => 'Пользователь',
			'ELEMENT' => 'Элемент инфоблока',
			'HL' => 'Запись highload блока',
			'TICKET' => 'Тикет тех. поддержки',
			'STAT_EVENT' => 'Событие статистики',
			'UF' => 'Пользовательские поля',
		);
		return $array;
	}
	
	/**
	 * Метод возвращает массив возможных типов сравнения значений
	 * @return array
	 */
	public static function getComparingTypes()
	{
		$array = array(
			'equal' => 'равно',
			'not_equal' => 'не равно',
			'preg_match' => 'preg_match'
		);
		return $array;
	}
	
	/**
	 * Если в массиве $item есть ключи и их значения удовлетворяют условиям исключения в $exclude,
	 * то метод вовзращает true, иначе - false
	 * @param array $item - входной массив для проверки
	 * @param array $exclude - массив с параметрами исключений
	 * @return array
	 */
	public static function checkExclude($item, $exclude)
	{
		$match = false;
		$result = array('SUCCESS' => $match);
		if(empty($exclude) || !is_array($exclude))
			return $result;
		
		foreach($exclude as $id => $arr)
		{
			if (strlen($arr['name']) == 0 || !array_key_exists($arr['name'], $item))
				continue;
			
			$match = self::compareValues($arr['type'], $arr['value'], $item[$arr['name']]);
			if($match)
			{				
				$result = array('SUCCESS' => $match, 'VALUE' => $item[$arr['name']], 'EXCLUDE' => $arr);
				break;
			}
		}		
		
		return $result;
	}
	
	public function compareValues($type, $val1, $val2)
	{
		$match = false;
		
		if($type == 'equal')
			$match = ($val1 == $val2);
		elseif($type == 'not_equal')
			$match = ($val1 != $val2);
		elseif($type == 'preg_match')
			$match = preg_match($val1, $val2);
			
		return $match;
	}
	
	/**
	 * Метод возвращает текст ошибки, вызванной функцией рег. выражений
	 * @return string
	 */
	public static function getRegexpError()
	{
		if (!preg_last_error())
			return false;
		else if (preg_last_error() == PREG_NO_ERROR) {
			return 'There is no error.';
		}
		else if (preg_last_error() == PREG_INTERNAL_ERROR) {
			return 'There is an internal error!';
		}
		else if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR) {
			return 'Backtrack limit was exhausted!';
		}
		else if (preg_last_error() == PREG_RECURSION_LIMIT_ERROR) {
			return 'Recursion limit was exhausted!';
		}
		else if (preg_last_error() == PREG_BAD_UTF8_ERROR) {
			return 'Bad UTF8 error!';
		}
		else if (preg_last_error() == PREG_BAD_UTF8_ERROR) {
			return 'Bad UTF8 offset error!';
		}		
	}
	
	/**
	 * Метод возвращает рег. выражение, описывающее один узел DOM-документа по тегу с его содержимым
	 *
	 * @param string $tagName - название тега
	 * @param boolean $groupContent - если true, то содержимое тега будет сгруппировано
	 * @return string
	 */
	public static function getTagRegexp($tagName, $groupContent = false)
	{
		$openGroup = ($groupContent ? '(' : '');
		$closeGroup = ($groupContent ? ')' : '');
		$regexp = '#\<'.$tagName.'[^\>]*\>'.$openGroup.'(?:(?!\<\s?/\s?'.$tagName.'\>).)*'.$closeGroup.'\<\s?/\s?'.$tagName.'\>#s';
		return $regexp;
	}
	
	/**
	 * Метод возвращает результат запроса по протоколу SOAP
	 *
	 * @param $method - имя метода
	 * @param $url - адрес веб-сервиса
	 * @param $arAuth - массив для аутентификации (login, password)
	 * @param $arData - массив с переменными для передачи в метод
	 * @return array
	 */	
	public function getSoapResponse($method, $url, $arAuth, $arData)
	{
		$result = array('ERROR' => true);
		
		if(empty($method) || empty($url) || empty($arData))
		{
			$result['TYPE'] = 'params';
			$result['MESSAGE'] = 'Wrong parameters';
			return $result;
		}
		
		try
		{
			ini_set('default_socket_timeout', 600);
			$client = new \SoapClient($url, $arAuth);			
			$result = (array)call_user_func_array(array($client, $method), array($arData));			
		}
		catch(\SoapFault $exception)
		{
			$result['TYPE'] = 'exception';
			$result['MESSAGE'] = $exception->getMessage();
		}
		
		return $result;	
	}
	
	/**
	 * Метод возвращает результат HTTP POST запроса
	 *
	 * @param $url - адрес
	 * @param $data - массив с переменными
     * @param $headers - массив для установки заголовков
	 * @return string
	 */	
	public function getHttpPostResponse($url, $data, $headers = null)
	{
		$h = new HttpClient();
                
		if ($headers)
		{
			foreach ($headers as $key => $header)
				$h->setHeader($key, $header);
		}
		
        $h->disableSslVerification();
		
		$response = $h->post($url, $data);
        
		$result = [
			'status' => $h->getStatus(), 
			'error' => $h->getError(),
			'body' => $response
		];
		
		return $result;		
	}
	
	/**
	 * Метод возвращает результат HTTP GET запроса
	 *
	 * @param $url - адрес
	 * @param $data - массив с переменными
     * @param $headers - массив для установки заголовков
	 * @return string
	 */	
	public function getHttpGetResponse($url, $data, $headers = null)
	{	
		$h = new HttpClient();
                
		if ($headers)
		{
			foreach ($headers as $key => $header)
				$h->setHeader($key, $header);
		}
		
        $h->disableSslVerification();
		
		$response = $h->get($url . '?' .http_build_query($data));
        
		$result = [
			'status' => $h->getStatus(), 
			'error' => $h->getError(),
			'body' => $response
		];
		
		return $result;		
	}
	
	/**
	 * Метод возвращает массив ключей массива данных из файла
	 * @param string $filePath
	 * @return array
	 */
	public static function getDataKeys($filePath)
	{		
		$json_data = self::getFileContents($filePath);
		$arJson = json_decode($json_data, true);
		return array_keys(current($arJson));
	}
	
	public static function isValidTimeStamp($timestamp)
	{
		return ((string) (int) $timestamp === $timestamp) 
			&& ($timestamp <= PHP_INT_MAX)
			&& ($timestamp >= ~PHP_INT_MAX);
	}
	
	public function getFileContents($path)
	{
		try
		{
			return IO\File::getFileContents($path);
		}
		catch(\Exception $e)
		{
			return false;
		}		
	}
	
	public function putFileContents($path, $data, $flags)
	{
		try
		{
			return IO\File::putFileContents($path, $data, $flags);
		}
		catch(\Exception $e)
		{
			return false;
		}		
	}
}