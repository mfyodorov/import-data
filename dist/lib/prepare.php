<?
namespace BitrixData;

use Bitrix\Main\Diag\Debug,
	Bitrix\Main\Loader,
	Bitrix\Main\Application;

class Prepare
{
	public static function getConfigFilePath()
	{
		return Main::getDirPath(true).'/data/prepare_config.json';
	}
	
	/**
	 * Метод возвращает массив доступных типов файлов
	 * @return array
	 */
	public static function getFileTypes()
	{
		$array = array(
			'csv_semicolon' => 'CSV (точка с запятой)',
			'csv_comma' => 'CSV (запятая)',
			'html_table' => 'HTML таблица',
			'json' => 'JSON',
		);
		return $array;
	}
	
	/**
	 * Метод возвращает массив ключей для возвращения значения поля выборки
	 * @return array
	 */	
	public function getFieldReturnKeys($object = 'ELEMENT')
	{
		$array = array();	
		switch($object)
		{
			case 'ELEMENT':
				$array[''] = '-- вернуть ключ --';
				$array['VALUE'] = 'VALUE';
				$array['DESCRIPTION'] = 'DESCRIPTION';
				$array['VALUE_XML_ID'] = 'XML_ID значения';
				$array['VALUE_ENUM_ID'] = 'ID варианта списка';
			break;
			case 'HL':
				$array[''] = '-- вернуть значение --';
			break;
		}
		return $array;
	}
	
	/**
	 * Метод возвращает доступные поля объекта
	 * @param string $object - тип объекта
	 * @return array
	 */
	public static function getObjectFields($object = 'ELEMENT')
	{
		$array = array(
			'' => '-- привязка --',
			'ID' => 'ID',
			'SORT' => 'Сортировка'
		);	
		switch($object)
		{
			case 'USER':
				$array['ACTIVE'] = 'Активность';
				$array['EMAIL'] = 'E-mail';
				$array['LOGIN'] = 'Логин';
				$array['NAME'] = 'Имя';
				$array['LAST_NAME'] = 'Фамилия';
				$array['WORK_COMPANY'] = 'Компания';
				$array['PERSONAL_PHONE'] = 'Телефон';
				$array['GROUP_ID'] = 'Группа';
				$array['XML_ID'] = 'XML_ID';
				$array['UF'] = 'UF свойство';
			break;
			case 'ELEMENT':
				$array['NAME'] = 'Название';
				$array['CODE'] = 'Символьный код';
				$array['XML_ID'] = 'XML_ID';
				$array['ACTIVE'] = 'Активность';
				$array['ACTIVE_FROM'] = 'Начало активности';
				$array['ACTIVE_TO'] = 'Окончание активности';
				$array['PREVIEW_TEXT'] = 'Текст анонса';
				$array['PREVIEW_PICTURE'] = 'Картинка анонса';
				$array['DETAIL_TEXT'] = 'Детальное описание';
				$array['DETAIL_PICTURE'] = 'Детальная картинка';
				$array['TAGS'] = 'Теги';
				$array['PROPERTY'] = 'Свойство';
			break;
			case 'HL':
				unset($array['SORT']);
				$array['UF'] = 'UF свойство';
			break;
			case 'TICKET':
				unset($array['SORT']);
				$array['TITLE'] = 'Заголовок';
				$array['OWNER_USER_ID'] = 'Автор (ID)';
				$array['STATUS_ID'] = 'Статус (ID)';
				$array['UF'] = 'UF свойство';
			break;
		}
		
		$array['INPUT'] = 'Произвольное поле';
		
		return $array;
	}

	/**
	 * Метод возвращает массив доступных методов обработки импортируемых полей
	 * @return array
	 */
	public static function getProcessingMethods()
	{
		$array = array(
			'Строка/число' => array(
				'' => '-- без обработки --',
				'int' => 'int',
				'trim' => 'trim',
				'bool' => 'bool',
				'numbers' => 'оставить только цифры',
				'strip_tags' => 'strip_tags',
				'html_entity_decode' => 'html_entity_decode',
				'nbsp' => 'удалить & nbsp;',
				'date' => 'формат даты',
				'regexp' => 'preg_replace',
				'variants' => 'варианты замены',
			),
			'Массив' => array(
				'array' => 'обернуть в массив',
				'array_item' => 'вернуть элемент массива',
				'explode' => 'explode',
				'implode' => 'implode',
				'serialize' => 'serialize',
				'unserialize' => 'unserialize',
			),
			'Битрикс' => array(
				'file' => 'получить ссылку на файл',
				'datetime' => 'преобразовать в DateTime',
				'iblock' => 'поиск в инфоблоке',
				'user' => 'поиск среди пользователей',
				'hl' => 'поиск в highload-блоке',
			),
			'КЛАДР' => array(
				'kladr_code_name' => 'КЛАДР: код -> название',
				'kladr_name_code' => 'КЛАДР: название -> код',
				'kladr_code_parent_code' => 'КЛАДР: код -> код родителя',
				'kladr_code_parent_name' => 'КЛАДР: код -> название родителя',
			)
		);
		return $array;
	}
	
	/**
	 * Метод класса, обрабатывающий ajax запросы
	 * @return array
	 */
	public static function ajax()
	{	
		if ($_POST['isAjax'])
		{
			global $APPLICATION;
			$APPLICATION->RestartBuffer();
			
			$result = array('SUCCESS' => false, 'MESSAGE' => 'Заполните обязательные поля');
			
			switch($_POST['action'])
			{
				case 'save_config':
					$config = array(
						'addValues' => $_POST['addValues'],
						'setFields' => $_POST['setFields'],
						'processingType' => $_POST['processingType'],
						'processingData' => $_POST['processingData'],
					);
					$json = json_encode(array('CONFIG' => $config, 'OBJECT' => $_POST['object'], 'FIELDS_COUNT' => $_POST['fieldsCount']));
					$res = file_put_contents(self::getConfigFilePath(), $json, LOCK_EX);
					if ($res === false)
						$result = array('SUCCESS' => false, 'MESSAGE' => 'Ошибка сохранения конфигурации');
					else
						$result = array('SUCCESS' => true, 'MESSAGE' => 'Конфигурация сохранена');
				break;
				case 'load_config':
					$result = array('SUCCESS' => false, 'MESSAGE' => 'Нет сохраненной конфигурации');
					$json = file_get_contents(self::getConfigFilePath());					
					if ($json)
					{
						$data = json_decode($json, true);
						if (is_array($data))
							$result = array(
								'SUCCESS' => true, 
								'DATA' => $data['CONFIG'], 
								'FIELDS_COUNT' => $data['FIELDS_COUNT'],
								'OBJECT' => $data['OBJECT']
							);
					}
				break;
				case 'preview':
					$data = self::createData($_POST);
					if ($data['SUCCESS'])
					{
						/** Отобразим и выделим в таблице предпросмотра неразрывный пробел, чтобы знать, где он прячется */
						$data['ARRAY'] = self::processFieldValueRec($data['ARRAY'], 'regexp', array('regexp' => '#^&nbsp;$#', 'newstring' => '<span class="key">& nbsp;</span>'));
						
						$result = array(
							'SUCCESS' => true, 
							'object' => $_POST['object'],
							'userFieldsList' => (!empty($_POST['object']) ? self::getObjectFields($_POST['object']) : ''),
							'procFieldsList' => self::getProcessingMethods(),
							'importFieldsValues' => $data['ARRAY'],
							'setFieldsValues' => $data['KEYS'],
							'curString' => $data['NUMBER'],
							'rowsCount' => $data['ROWS_COUNT'],
							'fieldsCount' => $data['FIELDS_COUNT']
						);
					}
					else $result = $data;
				break;
				case 'processValue':
					$val = $_POST['value'];
					if ($val && $_POST['processingType'])
					{
						foreach ($_POST['processingType'] as $row => $types)
						{
							foreach ($types as $k => $type)
							{
								$val = self::processFieldValueRec($val, $type, $_POST['processingData'][$row][$k]);
							}
						}
					}
					$result = array('SUCCESS' => true, 'VALUE' => $val, 'MESSAGE' => '');
				break;
			}
			
			die(json_encode($result));		
		}
	}
	
	public static function createData($params)
	{
		$result = array('SUCCESS' => false, 'MESSAGE' => 'Укажите источник данных');
		
		switch($params['source'])
		{
			case 'exists': 
				$data = Main::getData(); 
				$result = (
					isset($params['string']) 
					? self::getPreviewRow($data, $params['string'])
					: self::getResultArray($data, $params)
				);
			break;
			case 'file': 
				$result = self::getFileData($params); 
				if ($result['SUCCESS'])
				{
					$result = (
						isset($params['string']) 
						? self::getPreviewRow($result['DATA'], $params['string'], $result['KEYS'])
						: self::getResultArray($result['DATA'], $params)
					);
				}
			break;
			case 'bitrix':
				$result = self::getBitrixData($params);
				if ($result['SUCCESS'])
				{
					$result = (
						isset($params['string']) 
						? self::getPreviewRow($result['DATA'], $params['string'], $result['KEYS'])
						: self::getResultArray($result['DATA'], $params)
					);
				}
			break;
			case 'new':
				$previewRow = array(
					'SUCCESS' => true,
					'ARRAY' => array(),
					'NUMBER' => 1,
					'ROWS_COUNT' => 1,
				);
				$result = (
					isset($params['string']) 
					? $previewRow
					: self::getResultArray(array(array()), $params)
				);
			break;
		}
		
		return $result;
	}
	
	public function getBitrixData($params)
	{
		$result = array('SUCCESS' => false, 'MESSAGE' => 'Заполните обязательные параметры');		
			
		$filter = $select = $resultKeys = array();
		
		foreach ($params['bitrix_filter'] as $arr)
		{
			if ($arr['name'])
			{
				switch($arr['type'])
				{
					case 'int': $arr['value'] = intval($arr['value']); break;
					case 'array': $arr['value'] = explode(',',$arr['value']); break;
				}
				$filter[$arr['name']] = $arr['value'];
			}
		}
		
		/** If this is request for one element */
		if (isset($params['string']))
		{
			$isPreview = true;
			$params['string'] = intval($params['string']);
		}
		
		if (is_array($params['bitrix_select']))
		{
			foreach ($params['bitrix_select'] as $arr)
			{
				if ($arr['name'])
				{
					$needSelect = true;					
					
					preg_match('#^PROPERTY_(.+)$#', $arr['name'], $matches);
					if ($matches[1])
					{
						$needSelect = false;
						$arr['name'] = $matches[1];
					}
					
					$alias = ($arr['alias'] ?: $arr['name']);

					if ($needSelect)					
					{
						if ($arr['alias'])
							$select[$arr['alias']] = $arr['name'];
						else
							$select[] = $arr['name'];
					}
					
					$resultKeys[$alias] = ($arr['return'] ? $arr['return'] : 'VALUE');
				}
			}
		}
		$iterateMethod = 'Fetch';
		
		switch($params['bitrix_entity'])
		{
			case 'USER': 
				
				$queryParams = array('filter' => $filter, 'select' => $select, 'count_total' => true);
				if ($isPreview)
				{
					$queryParams['limit'] = 1;
					$queryParams['offset'] = ($params['string'] ? $params['string']-1 : 1);
				}	
				$res = \Bitrix\Main\UserTable::getList($queryParams);
				$rowsCount = $res->getCount();
				$res = (!$res ?: new \CDBResult($res));
				
			break;
			
			case 'ELEMENT': 
				if (!Loader::includeModule('iblock') || !$params['bitrix_block_id'])
					return $result;
				
				$filter['IBLOCK_ID'] = $params['bitrix_block_id'];
				$select[] = 'IBLOCK_ID';
				$select[] = 'ID';
				$resultKeys['ID'] = '';
				$limit = false;
				if ($isPreview)
				{
					$limit['nPageSize'] = 1;
					$limit['iNumPage'] = ($params['string'] ?: 1);
				}	
				
				$res = \CIBlockElement::GetList(array('sort' => 'asc'), $filter, false, $limit, $select);
				$iterateMethod = 'GetNextElement';
				
			break;
			case 'HL': 
			
				if (!$params['bitrix_block_id'])
					return $result;
				
				if (!$select)
					$select = array('*');
				
				$queryParams = array('filter' => $filter, 'select' => $select, 'count_total' => true);
				if ($isPreview)
				{
					$queryParams['limit'] = 1;
					$queryParams['offset'] = ($params['string'] ? $params['string']-1 : 1);
				}	
				$res = HLBlocks::execQueryHL($params['bitrix_block_id'], $queryParams);
				$rowsCount = $res->getCount();
				$res = (!$res ?: new \CDBResult($res));
				
			break;
			case 'TICKET': 
				if (!Loader::includeModule('support'))
					return $result;
				
				$queryParams = array('SELECT' => array('UF_*'));
				
				if ($isPreview)
				{
					$queryParams['NAV_PARAMS']['nPageSize'] = 1;
					$queryParams['NAV_PARAMS']['iNumPage'] = ($params['string'] ?: 1);
				}
				
				$res = \CTicket::GetList($order = 's_id', $sort = 'desc', $filter, $is_filtered, 'N', 'Y', 'Y', false, $queryParams);
				
			break;
			case 'STAT_EVENT': 
				
				$select[] = 'ID';
				$resultKeys['ID'] = '';
				
				if ($isPreview)
				{
					$limit['nPageSize'] = 1;
					$limit['iNumPage'] = ($params['string'] ?: 1);
				}	
				
				$res = \CStatEvent::GetList($by = 's_id', $order = 'asc', $filter, $isFiltered);
				
			break;
			default: 
				return $result;
			break;
		}
		
		$data = array();
		if ($res)
		{
			$rowsCount = (isset($rowsCount) ? $rowsCount : $res->SelectedRowsCount());
			
			/** Returns last element if number of selected element more than all count */
			if ($params['string'] > $rowsCount)
			{
				$params['string'] = $rowsCount;
				return self::getBitrixData($params);
			}
			
			if (!$params['bitrix_limit'] || $params['bitrix_limit'] > $rowsCount)
				$params['bitrix_limit'] = $rowsCount;
			
			$i = 1;
			while($ob = call_user_func_array(array($res, $iterateMethod), array(false, false)))
			{
				if (is_array($ob))
				{
					$arr = $ob;
					unset($ob);
				}
				else
				{
					$arr = $ob->GetFields();
					$arr = array_merge($arr, $ob->GetProperties());
				}
				
				if ($isPreview && $params['string'] > 1)
				{
					$offset = $params['string']-1;
					$data = array_fill(0, $offset, array());
				}
				
				$values = array();
				
				/** Get just selected fields values or all if selected are not set */
				
				if ($resultKeys)
				{
					foreach ($resultKeys as $key => $return)
					{
						$value = (is_array($arr[$key]) ? $arr[$key][$return] : $arr[$key]);
						
						if (is_a($value, 'Bitrix\Main\Type\DateTime'))
						{
							$value = $value->getTimestamp();
						}
						
						$values[] = $value;
					}
					$keys = array_keys($resultKeys);
				}
				else
				{
					$values = array_values($arr);
					$keys = array_keys($arr);
				}
				
				$data[] = $values;
				
				if ($isPreview && $params['bitrix_limit'] > count($data))
				{
					$empty = array_fill(count($data), $params['bitrix_limit']-count($data), array());
					$data = array_merge($data, $empty);
					break;
				}
				
				if ($i == $params['bitrix_limit'])
					break;
				
				++$i;
			}		
		}
		
		return array('SUCCESS' => true, 'DATA' => $data, 'KEYS' => $keys);
	}
	
	/**
	 * Метод разбирает содержимое файла в зависимости от его типа и возвращает массив данных
	 *
	 * @param array $params - параметры
	 * @return array
	 */
	public static function getFileData($params)
	{
		$filePath = ($_FILES['file']['tmp_name'] ? $_FILES['file']['tmp_name'] : $params['file_url']);
		
		if (!$filePath)
			return array('SUCCESS' => false, 'MESSAGE' => 'Файл не выбран или не указан путь до файла');
		
		$result = array('SUCCESS' => false, 'MESSAGE' => 'Не удалось прочитать файл');
		
		$fileContent = file_get_contents($filePath);
		
		if ($fileContent === false)
			return $result;
		elseif (empty($fileContent))
			return array('SUCCESS' => false, 'MESSAGE' => 'Файл пуст');
		
		$arFile = $keys = array();
		
		/**
		 * Разбор данных в зависимости от типа файла
		 */	
		if (preg_match('#^csv_#', $params['file_type']))
		{
			// Debug::writeToFile(mb_detect_encoding($fileContent));
			// $fileContent = mb_convert_encoding($fileContent, 'utf-8');
			
			$rows = explode("\n", $fileContent);
			$delimiter = ($params['file_type'] == 'csv_comma' ? ',' : ';');

			if (empty($rows) || strpos(current($rows), $delimiter) === false)
				return $result;	
			
			foreach ($rows as $i => $row)
			{				
				$arFile[$i] = explode($delimiter, $row);			
			}		
		}	
		elseif ($params['file_type'] == 'html_table')
		{
			/** Если таблиц несколько - работаем только с первой */
			/*
			$tableIndex = 0;
			preg_match_all(getTagRegexp('table'), $fileContent, $tables);
			if ($tables[0][$tableIndex])
			{
				preg_match_all(getTagRegexp('tr'), $tables[0][$tableIndex], $trs);
				if ($trs[0])
				{
					foreach ($trs[0] as $i => $tr)
					{
						preg_match_all(getTagRegexp('td'), $tr, $tds);
						$arFile[$i] = $tds[0]; // под 1 индексом - только контент ячейки
					}			
				}
			} 
			*/
			
			$tables = preg_split('#\</?table[^\>]*\>#', $fileContent);
			$tables = array_filter($tables, 'self::isTableContent');
			$tableContent = current($tables);
			$trs = preg_split('#\<tr[^\>]*\>#', $tableContent);
			$arFile = array();
			$i = 0;
			foreach ($trs as $tr)
			{
				if (!$firstExcluded && empty($tr))
				{
					$firstExcluded = true;
					continue;
				}
				$tr = preg_replace('#\</tr\>#', '', $tr);
				$tds = preg_split('#\<td[^\>]*\>#', $tr);
				$k = 0;
				foreach ($tds as $td)
				{			
					if (!$firstExcluded && empty($td))
					{
						$firstExcluded = true;
						continue;
					}
					$arFile[$i][$k] = preg_replace('#\</td\>#', '', $td);
					++$k;
				}
				++$i;
			}
			if (preg_last_error())
				$result['MESSAGE'] = 'Regexp error: '.Main::getRegexpError();
		}
		elseif ($params['file_type'] == 'json')
		{
			$arFile = json_decode($fileContent, true);
			
			/** Массив каждой строки не должен быть ассоциативным, т.е. должен иметь числовые ключи */
			foreach ($arFile as &$row)
			{
				if (!$keys)
					$keys = array_keys($row);
				$row = array_values($row);
			}
		}
		
		if (!$arFile)
			return $result;
		else
			return array('SUCCESS' => true, 'DATA' => $arFile, 'KEYS' => $keys);		
	}
	
	/**
	 * Метод возвращает из массива данных срез под номером $numberRow
	 */	
	public function getPreviewRow($data, $numberRow, $keys = array())
	{
		$numberRow = intval($numberRow);
		$rowsCount = count(array_keys($data));
		if ($numberRow < 1)
			$numberRow = 1;
		elseif ($numberRow > $rowsCount)
			$numberRow = $rowsCount;		
		$key = $numberRow - 1;
		
		foreach ($data[$key] as &$item)
		{
			if (strlen($item) > 200)
				$item = mb_substr($item, 0, 200).' [...]';
		}
		return array(
			'SUCCESS' => true,
			'ARRAY' => $data[$key],
			'KEYS' => $keys,
			'NUMBER' => $numberRow,
			'ROWS_COUNT' => $rowsCount,
			'FIELDS_COUNT' => count($data[$key])
		);
	}
	
	
	public function getResultArray($data, $params)
	{
		/** Обход сформированного массива */
		
		$arExclude = (!empty($params['exclude_rows']) ? explode(',', $params['exclude_rows']) : array());
		foreach ($arExclude as $k => $val)
		{
			$arExclude[$k] = intval($val);
			if (!$arExclude[$k])
				unset($arExclude[$k]);
		}
		
		$arJson = array();
		foreach ($data as $i => $row)
		{		
			/** Исключаем указанные строки по номеру */
			if (in_array($i+1, $arExclude))
				continue;
			
			/** Ограничение кол-ва строк  */
			if ($params['limit_items'] && intval($params['limit_items']) == $i) 
				break;			

			/** Проверяем условия на исключение */
			$exclude = Main::checkExclude($row, $params['exclude']);
			if ($exclude['SUCCESS'])
				continue;
			
			foreach ($_POST['setFields'] as $key => $fieldName)
			{
				if (!empty($fieldName))
				{
					/** Если текущий индекс больше кол-ва импортируемых полей, значит это дополнительное поле пользователя */
					if (count($row) > $key)
					{
						$value = $row[$key];
					}
					else
					{
						/** Если передаваемое значение имеет вид "COPY:$i", значит это - копия поля под индексом $i */
						preg_match('#^COPY:(\d+)$#', $_POST['addValues'][$key], $matches);
						
						if ($matches)
							$value = $row[$matches[1]];
						else
							$value = $_POST['addValues'][$key];
					}
					
					/** Обработка значения */
					if (!empty($_POST['processingType'][$key]))
					{
						foreach ($_POST['processingType'][$key] as $k => $type)
						{
							if (!$type)
								continue;
							
							$value = self::processFieldValueRec($value, $type, $_POST['processingData'][$key][$k]);
						}
					}
					
					$arJson[$i][$fieldName] = $value;
				}
			}
		}		
		
		return array('SUCCESS' => true, 'DATA' => $arJson);	
	}
	
	/**
	 * Метод используется в createData
	 * @param string $content
	 * @return boolean
	 */
	public static function isTableContent($content)
	{
		return preg_match('#^\<tr[^\>]*\>.*\</tr\>$#s', $content);
	}
	
	/**
	 * Функция рекурсивно обрабатывает переменную $value с помощью метода processFieldValue
	 *
	 * @param mixed $value переменная для обработки
	 * @param string $type тип обработки
	 * @param array $data параметры обработки
	 * @return mixed
	 */	
	public static function processFieldValueRec($value, $type, $data)
	{
		if (!$type)
			return $value;
		
		$notRecursive = array(
			'implode' => true,
			'array_item' => true
		);
		
		$isArray = is_array($value);
		if (!$isArray || $notRecursive[$type])
			$value = array($value);
		
		foreach ($value as $k => $item)
		{
			if (is_array($item) && !$notRecursive[$type])
				$value[$k] = self::processFieldValueRec($item, $type, $data);
			else
				$value[$k] = self::processFieldValue($item, $type, $data);
		}
		
		return ($isArray && !$notRecursive[$type] ? $value : $value[0]);
	}
	
	/**
	 * Функция обрабатывает строковую переменную в зависимости от переданного типа обработки
	 *
	 * @param string|int $item переменная для обработки
	 * @param string $type тип обработки
	 * @param array $data параметры обработки
	 * @return mixed
	 */
	public static function processFieldValue($item, $type, $data)
	{
		switch($type)
		{
			case 'int': 
				$item = (int)$item; 
			break;
			case 'trim': 
				$item = trim($item); 
			break;
			case 'bool': 
				$item = (bool)$item; 
			break;
			case 'strip_tags': 
				$item = strip_tags($item); 
			break;
			case 'html_entity_decode': 
				$item = html_entity_decode($item); 
			break;
			case 'numbers': 
				$item = (int)preg_replace('/\D/','',$item); 
			break;
			case 'date':
				$format = ($data['format'] ? $data['format'] : 'Y-m-d H:i:s');
				if (!empty($item))
				{
					$item = (string) $item;
					$timestamp = (Main::isValidTimeStamp($item) ? $item : strtotime($item)); 
					$item = date($format, $timestamp); 
				}
			break;
			case 'array': 
				$item = array($item); 
			break;
			case 'array_item': 
				$item = $item[$data['key']]; 
			break;
			case 'explode': 
				if ($data['delimiter'])
					$item = (!empty($item) ? explode($data['delimiter'], $item) : array());
			break;
			case 'implode': 
				if ($data['delimiter'] && is_array($item))
					$item = implode($data['delimiter'], $item);
			break;
			case 'serialize': 
				$item = serialize($item);
			break;
			case 'unserialize': 
				$item = unserialize($item);
			break;
			case 'regexp': 
				if (!empty($data['regexp']))
				{
					$regexp = $data['regexp'];
					$newstring = (!empty($data['newstring']) ? $data['newstring'] : '');
					$item = preg_replace($regexp, $newstring, $item);
				}
			break;
			case 'nbsp': 
				$item = preg_replace('#&\s?nbsp;#', '', $item);
			break;
			case 'variants':
				$match = false;
				if (isset($data['before']))
				{
					$type = ($data['type'] ? $data['type'] : 'equal');
					$before = explode('/', $data['before']);
					$after = explode('/', $data['after']);
					foreach ($before as $k => $var)
					{
						$match = Main::compareValues($type, trim($var), $item);
						if ($match)
						{
							$item = trim($after[$k]);
							break;
						}
					}					
				}
				if (!$match)
					$item = '';
			break;
			case 'kladr_code_name': 
				$arKladr = self::getKladrItem($item, $data['kladr_type']);
				if (!empty($arKladr[0]))
					$item = $arKladr[0]['name'].'///'.$arKladr[0]['typeShort'];
			break;
			case 'kladr_name_code': 
				$arKladr = self::getKladrItem($item, $data['kladr_type']);
				if (!empty($arKladr[0]))
					$item = $arKladr[0]['id'];
			break;
			case 'kladr_code_parent_code': 
			case 'kladr_code_parent_name': 
				if (!empty($data['kladr_parent_type']))
				{
					$arKladr = self::getKladrItem($item, $data['kladr_type'], true);				
					/** Выдаем в качестве региона для городов Москва и СПб самих себя (т.к. КЛАДР не находит для них региона) */
					if (
						$data['kladr_type'] == 'city' && $data['kladr_parent_type'] == 'region' 
						&& (preg_match('#^770{11,}$#', $item) || preg_match('#^780{11,}$#', $item))
					)
					{
						$item = ($type == 'kladr_code_parent_code' ? $arKladr[0]['id'] : $arKladr[0]['name'].'///'.$arKladr[0]['typeShort']);
					}
					elseif (!empty($arKladr[0]['parents']))
					{
						foreach ($arKladr[0]['parents'] as $arParent)
						{
							if ($arParent['contentType'] == $data['kladr_parent_type']) 
								$item = ($type == 'kladr_code_parent_code' ? $arParent['id'] : $arParent['name'].'///'.$arParent['typeShort']);
						}					
					}
				}
			break;
			case 'iblock': 
			case 'user':
			case 'hl':  
				$item = self::getBitrixValue($item, $type, $data);				
			break;
			case 'file':
				$path = \CFile::GetPath(intval($item));
				if ($path)
				{
					$request = Application::getInstance()->getContext()->getRequest();
					$protocol = ($request->isHttps() ? 'https' : 'http');
					$item = sprintf('%s://%s%s', $protocol, $request->getHttpHost(), $path);	
				}		
			break;
			case 'datetime':
				$item = new \Bitrix\Main\Type\DateTime($item);	
			break;
		}
		
		return $item;
	}
	
	public static function getBitrixValue($item, $type, $data)
	{
		if (!$data['param'] || !$data['select'])
			return $item;
		
		$select = array($data['select']);		
		$filter = array($data['param'] => $item);
		
		foreach ($data['filter'] as $item)
			if ($item['key'])
				$filter[$item['key']] = $item['value'];
		
		switch($type)
		{
			case 'iblock':
				if (!Loader::includeModule('iblock') || !$data['block_id']) 
					break;
				$select[] = 'IBLOCK_ID';
				$select[] = 'ID';
				$filter['IBLOCK_ID'] = $data['block_id'];
				$res = \CIBlockElement::GetList(array(), $filter, false, false, $select);
			break;
			case 'user':
				$params = (preg_match('#^UF_#', $data['select']) ? array('SELECT' => $select, 'FIELDS' => array('ID')) : array('FIELDS' => $select));
				$res = \CUser::GetList(($by = ''), ($order = ''), $filter, $params);				
			break;
			case 'hl':
				if (!$data['block_id']) 
					break;				
				$res = HLBlocks::execQueryHL($data['block_id'], array('filter' => $filter, 'select' => $select));
				$res = (!$res ?: new \CDBResult($res));
			break;
		}
		
		if ($res)
		{
			$item = '';
			if ($arr = $res->GetNext(false,false))
			{
				$item = (
					preg_match('#^PROPERTY_#', $data['select'])
					? $arr[strtoupper($data['select']).'_VALUE']
					: $arr[$data['select']]
				);
			}
		}
		
		return $item;
	}
	
	/**
	 * Метод обращается к веб-сервису КЛАДРа и возвращает необходимую информацию
	 *
	 * @param string $code - код объекта по КЛАДР
	 * @param string $type - тип объекта
	 * @param boolean $withParent - если true, так же будет возвращет предок данного объекта
	 * @return 
	 */
	public static function getKladrItem($code, $type, $withParent = false)
	{
		if (!in_array($type, array('region','district','city','street','building'))) return false;
		
		$url = 'http://kladr-api.ru/api.php';
		$arParams = array(
			'token' => '51dfe5d42fb2b43e3300006e',
			'key' => '86a2c2a06f1b2451a87d05512cc2c3edfdf41969',
			'limit' => 10,
			'contentType' => $type
		);
		if ($withParent) $arParams['withParent'] = 1;
		
		if (preg_match('#^\d{13,}$#', $code))
		{
			$len = strlen($code);
			$code = (
				in_array($type, array('region', 'city')) && $len > 13 
				? substr($code, 0, 13) 
				: $code
			);
			$arParams[$type.'Id'] = $code;
		}
		else
		{
			$arParams['query'] = $code;
		}
		
		$url = $url.'?'.http_build_query($arParams);
		$json = file_get_contents($url);
		$arResult = json_decode($json, true);
		
		return $arResult['result'];
	}
	
	public function processDataArray($data, $params)
	{
		$result = array('SUCCESS' => false, 'MESSAGE' => 'Укажите источник данных');
		
		/** Create arrays for collect unique values */
		$uniqueValues = array();
		foreach ($params['unique_fields'] as $field)
		{
			if ($field['name'])
				$uniqueValues[$field['name']] = array();
		}
		
		foreach ($data as $k => $item)
		{
			foreach ($uniqueValues as $fieldName => $fieldValues)
			{
				if ($item[$fieldName])
				{
					if (in_array($item[$fieldName], $fieldValues))
					{
						unset($data[$k]);
					}
					else $uniqueValues[$fieldName][] = $item[$fieldName];
				}
			}
			
			foreach ($params['compare_fields'] as $cond)
			{
				if (!$cond['name1'] || !$cond['name2'])
					continue;
				
				if ($item[$cond['name1']] === $item[$cond['name2']])
					unset($data[$k]);
			}
		}
		
		return array('SUCCESS' => true, 'DATA' => $data);
	}
}