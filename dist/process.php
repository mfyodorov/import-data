<?
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Diag\Debug,
	Bitrix\Main\Application,
	Bitrix\Main\Type;

$request = Application::getInstance()->getContext()->getRequest();

/** Assets, navigation */
require_once('inc/header.php');

/**
 * Starting process
 */	
if($request['run'] == 'Y'):

	$time['start'] = new Type\DateTime();
	$data = BitrixData\Main::getData();	
	$result = BitrixData\Prepare::processDataArray($data, $params);	
	$time['end'] = new Type\DateTime();
?>
	<p>Процесс завершен: <?=$time['start']->format('H:i:s')?> &mdash; <?=$time['end']->format('H:i:s')?></p>
		
	<?if($result['SUCCESS']):
		$res = BitrixData\Main::setData($result['DATA']);
	?>
		<?if($res !== false):?>
			<p>Массив данных обновлен. Было строк: <?=count($data)?>, стало: <?=count($result['DATA'])?></p>
		<?else:?>
			<p>Ошибка записи</p>
		<?endif;?>
		
	<?else:?>
		<p>Не удалось обработать данные</p>
	<?endif;?>
	
	<p>-----------------------------------------</p>
	
<?endif;?>

<?
$dataKeys = BitrixData\Main::getDataKeys(BD_DATA_FILE);
?>
<form class="main-form" method="POST">
	<input type="hidden" name="run" value="Y" />
	<fieldset>
		<div class="title">Массив данных</div>
		<div class="row row-medium">
			<label for="">Исключить элементы с повторяющимися значениями поля</label>
			<div class="repeat-group">
				<?foreach($params['unique_fields'] as $id => $arr):?>
					<div class="item" data-id="<?=$id?>">
						<select name="unique_fields[<?=$id?>][name]" class="medium">
							<option value="">-- ключ поля --</option>
							<?foreach($dataKeys as $key):?>
								<option value="<?=$key?>" <?if($arr['name'] == $key) echo 'selected';?>><?=$key?></option>
							<?endforeach;?>
						</select>
					</div>
				<?endforeach;?>
				<span class="add add-field" title="Добавить">+</span>
			</div>
		</div>
		<div class="row row-medium">
			<label for="">Исключить элементы с одинаковыми значениями полей</label>
			<div class="repeat-group">
				<?foreach($params['compare_fields'] as $id => $arr):?>
					<div class="item" data-id="<?=$id?>">
						<select name="compare_fields[<?=$id?>][name1]" class="medium">
							<option value="">-- ключ поля --</option>
							<?foreach($dataKeys as $key):?>
								<option value="<?=$key?>" <?if($arr['name1'] == $key) echo 'selected';?>><?=$key?></option>
							<?endforeach;?>
						</select>
						<select name="compare_fields[<?=$id?>][name2]" class="medium">
							<option value="">-- ключ поля --</option>
							<?foreach($dataKeys as $key):?>
								<option value="<?=$key?>" <?if($arr['name2'] == $key) echo 'selected';?>><?=$key?></option>
							<?endforeach;?>
						</select>
					</div>
				<?endforeach;?>
				<span class="add add-field" title="Добавить">+</span>
			</div>
		</div>
	</fieldset>	
	<input type="submit" value="Обработать массив" />
</form>

<form action="" method="POST">
	<fieldset>
		<p><span class="pseudo-link" onclick="copyToClipboard('<?=BD_DATA_FILE_URL?>')">Ссылка на файл</span></p>
		<p><a href="<?=BD_DATA_FILE_URL?>" target="_blank" class="pseudo-link" download>Скачать файл</a></p>
	</fieldset>	
	<?if($request['print_array'] == 'Y'):?>
		<button type="submit">Скрыть текущий массив</button>
	<?else:?>	
		<button type="submit" name="print_array" value="Y">Распечатать текущий массив</button>
	<?endif;?>	
</form>

<?if($request['print_array'] == 'Y'):
	$json_data = file_get_contents(BD_DATA_FILE);
?>
	<pre><?print_r(json_decode($json_data));?></pre>
<?endif;?>

<script>
$(function() {	
	//prepareData.initProcess();	
});
</script>